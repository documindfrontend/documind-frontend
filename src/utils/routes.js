export class Routes {
  static documentsPage = '/documents'
  static bulkDocumentsUpload = "/documents/bulk-documents-upload";


  static selectFolder = "/select-folder";
  static addDocuments = "/add-documents";
  static addCsv = "/add-csv";
  static  chooseProcess = "/choose-process";
  static  documentSettings = "/document-settings";
  static openedDocument = "/documents/id"

}
