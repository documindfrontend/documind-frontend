import { DATA_LOADED } from "../action-types/";

const dataLoaded = (newData) => {
  return {
    type: DATA_LOADED,
    payload: newData,
  };
};

export {
    dataLoaded
}