import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import App from 'app';
import ErrorBoundry from 'app/components/error-boundry';
import store from 'app/store';

// import 'bootstrap/dist/css/bootstrap.min.css'
import './fonts/Roboto-Regular.ttf';

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundry>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ErrorBoundry>
  </Provider>,
  document.getElementById('root')
);
