const initialState = {
  books: [
    // {
    //   id: 1,
    //   name: "STORE Bilbo Baggins [TEST DATA]",
    //   gender: "male",
    //   birthYear: "long ago",
    //   eyeColor: "dark brown",
    // },
    // {
    //   id: 2,
    //   name: " STORE Frodo Baggins [TEST DATA]",
    //   gender: "male",
    //   birthYear: "long ago",
    //   eyeColor: "dark brown",
    // },
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "DATA_LOADED":
      return {
        books: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
