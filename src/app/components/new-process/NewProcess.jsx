import React from 'react';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import CreateIcon from '@material-ui/icons/Create';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

export default function NewProcess() {
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      overflow: 'hidden',
      padding: theme.spacing(0, 3),
      marginTop: '10px',
      display: 'flex',
    },
    documentView: {
      height: '42em',
      justifyContent: 'center',
      display: 'flex',
      flexDirection: 'column',
      marginRight: '25px',
      marginLeft: '25px',
      marginTop: '20px',
      boxShadow:
        '4px 6px 5px -1px rgba(0,0,0,0.2), 0px 1px 33px 0px rgba(0,0,0,0.14), 0px 1px 16px 0px rgba(0,0,0,0.12)',
    },
    button: {
      height: '30px',
    },
  }));

  const classes = useStyles();

  const [value, setValue] = React.useState('female');

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Grid xs={12} className="root">
      <Grid xs={12} style={{ marginBottom: '15px' }}>
        <Typography>
          <h1 style={{ margin: 'initial' }}>Podpisywany dokument</h1>
        </Typography>
        <Typography>
          <h4 style={{ margin: 'initial' }}>
            Przewin dokument do konca, aby potwierdzić zapoznanie sie z nim{' '}
          </h4>
        </Typography>
      </Grid>
      <Grid style={{ display: 'flex' }}>
        <Grid xs={7} style={{ display: 'flex', justifyContent: 'center' }}>
          {/* <Card style={{}} className={classes.documentView}> */}
          {/* <Stepper /> */}
          {/* </Card> */}
        </Grid>
        <Grid
          xs={5}
          style={{
            display: 'flex',
            flexDirection: 'column',
            marginRight: '15px',
          }}
        >
          <Grid>
            <h2>Informacje</h2>
            <div>
              {' '}
              Dokumnet stworzony przez:{' '}
              <span>
                <b>Firma ABC Sp.z o. o.</b>
              </span>
            </div>
            <div>
              {' '}
              Data wyslania do podpisu:{' '}
              <span>
                <b>28 sirpnia 2020 13:22</b>
              </span>
            </div>
            <div>
              {' '}
              Data waznosci:{' '}
              <span>
                <b>28 sirpnia 2020 13:22</b>
              </span>
            </div>
          </Grid>
          <Grid>
            <h2>Metoda podpisu</h2>
            <div>
              {' '}
              Wybierz najdogodniejszą dla Ciebie metodę podpisu dokumentu.
              Dostępne są te tylko te dokumenty, do których autor udzielił
              dostępu.
            </div>
          </Grid>
          <Grid style={{ display: 'flex', flexDirection: 'column' }}>
            <FormControl component="fieldset">
              {/* <FormLabel component="legend">Gender</FormLabel> */}
              <RadioGroup
                aria-label="gender"
                name="gender1"
                value={value}
                onChange={handleChange}
              >
                <FormControlLabel
                  value="female"
                  control={<Radio />}
                  label="eDo App"
                />
                {/* <FormControlLabel value="male" control={<Radio />} label="Male" />
        <FormControlLabel value="other" control={<Radio />} label="Other" />
        <FormControlLabel value="disabled" disabled control={<Radio />} label="(Disabled option)" /> */}
              </RadioGroup>
            </FormControl>
            <Button
              size="small"
              variant="contained"
              className={classes.button}
              style={{
                color: 'white',
                backgroundColor: 'green',
                width: '100px',
              }}
              startIcon={<CreateIcon />}
            >
              Podpisz
            </Button>
            {/* <Chip /> */}
            {/* <h4>Dane w dokumencie </h4>{" "}
            <Button
              variant="outlined"
              className={classes.button}
              style={{ color: "green" }}
            >
              Dodaj nowe pole
            </Button>
            <div>Brak danuch </div> */}
          </Grid>
          <Grid style={{ marginTop: '15em' }}>
            <div>
              Jeżeli nie akceptujesz treści dokumentu, poinformuj o tym autora
              odrzucając dokument poniżej:
            </div>
            <Button
              size="small"
              variant="outlined"
              className={classes.button}
              style={{
                color: 'red',
                // backgroundColor: "red",
                // width: "100px",
              }}
              startIcon={<HighlightOffIcon />}
            >
              Odrzuć dokument
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {/* <Paper style={{ display: "flex", height: "100%" }}> */}

      {/* </Paper> */}
    </Grid>
  );
}
