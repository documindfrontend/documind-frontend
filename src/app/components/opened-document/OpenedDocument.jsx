import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router';
import { Routes } from 'utils/routes';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
// import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import backIcon from 'assets/backIcon.png';

import { makeStyles } from '@material-ui/core/styles';
// import Typography from '@material-ui/core/Typography';
// import AssignmentIcon from '@material-ui/icons/Assignment';
// import { Link } from 'react-router-dom';
// import DeleteSweepOutlinedIcon from '@material-ui/icons/DeleteSweepOutlined';
// import ShareButton from '../share-button/ShareButton';
import MenuItem from '@material-ui/core/MenuItem';
import Alert from '@material-ui/lab/Alert';
// import InputLabel from '@material-ui/core/InputLabel';
import Snackbar from '@material-ui/core/Snackbar';

// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { EditingDocument } from 'app/components/editing-document/EditingDocument';
import { useDispatch, useSelector } from 'react-redux';
import {
  // deleteCatalog,
  postGuidsForSign,
} from 'app/store/catalogs';
import {
  // addNewCatalog,
  // fetchDocument,
  selectBulkDocState,
  fetchDocumentAcroFields,
} from 'app/store/bulk-documents-upload';

import './OpenedDocument.scss';

export const OpenedDocument = () => {
  const bulkDocState = useSelector(selectBulkDocState);
  const dispatch = useDispatch();
  // const [age, setAge] = React.useState('');
  const [alertData, setAlertData] = useState();
  // const [podpis, setPodpis] = useState();
  const [editDocument, setEditDocument] = useState(false);
  const [viewerUrl, setViewerUrl] = useState('');
  const showHistory = !editDocument && bulkDocState.selectedDocumentData;
  const history = useHistory();
  // const [showAddNewFolderDialog, setShowAddNewFolderDialog] = useState(false);
  const acroFields = bulkDocState.selectedDocumentAcroFields;
  // console.log('bulkDocState', bulkDocState);

  const createdDate = bulkDocState.selectedDocuments[0].creationDate;
  const lastModDate = bulkDocState.selectedDocuments[0].lastModified;

  const preparedDate = (date) => {
    return date.replace(/T/g, ' ').split('.')[0];
  };

  const getDocumentAcroFields = () => {
    dispatch(fetchDocumentAcroFields(bulkDocState.selectedDocuments[0].documentId));
  };

  const onHandelEditDocument = async () => {
    await setEditDocument(true);
    await getDocumentAcroFields();
    console.log('setEditDocument', true);
  };

  const onHandelBackClick = () => {
    const url = `${Routes.documentsPage}`;
    history.push(url);
  };
  const getPdfUrl = useCallback(async () => {
    if (!bulkDocState.selectedDocuments[0].documentBase64) {
      return;
    }
    const blob = await fetch(
      `data:application/pdf;base64,${bulkDocState.selectedDocuments[0].documentBase64}`
    ).then((res) => res.blob());

    const url = URL.createObjectURL(blob);
    const viewerUrl =
      '/pdfjs-dist/web/viewer.html?file=' + encodeURIComponent(url);
    setViewerUrl(viewerUrl);
  }, [bulkDocState.selectedDocuments[0].documentBase64]);

  useEffect(() => {
    getPdfUrl();
  }, [getPdfUrl]);

  // const handleChange = (event) => {
  //   setAge(event.target.value);
  // };
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      overflow: 'hidden',
      padding: theme.spacing(0, 3),
      marginTop: '10px',
      display: 'flex',
    },
    documentView: {
      // height: '42em',
      justifyContent: 'center',
      display: 'flex',
      marginRight: '25px',
      marginLeft: '25px',
      marginTop: '20px',
      boxShadow:
        '4px 6px 5px -1px rgba(0,0,0,0.2), 0px 1px 33px 0px rgba(0,0,0,0.14), 0px 1px 16px 0px rgba(0,0,0,0.12)',
    },
    button: {
      // height: '30px',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

  const classes = useStyles();

  // const onDeleteDocument = () => {};

  // ----------
  const onStartProcess = () => {
    dispatch(postGuidsForSign(bulkDocState.selectedDocuments[0].documentId));

    setAlertData({
      errorType: 'success',
      message: 'Sukces, proces rozpoczęty !',
    });
  };

  return (
    <div className="root">
      <div
        style={{ display: 'flex', alignItems: 'center', paddingBottom: '10px' }}
      >
        <Button
          variant="outlined"
          color="default"
          className="button"
          onClick={() => onHandelBackClick()}
        >
          <img
            src={backIcon}
            alt="backIcon"
            style={{ width: '15px', marginRight: '5px' }}
          />
          Powrót
        </Button>
        <span
          style={{ fontSize: '25px', paddingLeft: '50px' }}
          className="page-title"
        >
          Dokumenty
        </span>
      </div>
      <Paper style={{ display: 'flex', height: '80vh', padding: '15px' }}>
        {viewerUrl && (
          <div style={{ flex: '1' }}>
            <iframe
              title={'document'}
              src={viewerUrl}
              type="application/pdf"
              style={{ width: '100%', height: '100%' }}
            />
          </div>
        )}
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            paddingLeft: '20px',
            flex: '2',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'start',
              alignItems: 'center',
              marginBottom: '1em',
            }}
          >
            <h2>{bulkDocState.selectedDocuments[0].documentName}</h2>
          </div>

          {/* <div style={{ display: 'flex' }}>
              <ShareButton />
              <Button
                size="small"
                variant="outlined"
                color="secondary"
                className={classes.button}
                style={{ border: '0px', minWidth: '20px' }}
                onClick={() => onDeleteDocument()}
              >
                <DeleteSweepOutlinedIcon />
              </Button>
            </div> */}
          {editDocument === false ? (
            <div>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  height: '5em',
                  marginBottom: '1em',
                }}
              >
                <span style={{ fontSize: '18px', fontWeight: '600' }}>
                  Proces
                </span>
                {/* <Link
              to="/documents/id/new-process"
              style={{ textDecoration: 'none' }}
            > */}
                <div style={{ display: 'flex', justifyContent: 'start' }}>
                  <Button
                    variant="outlined"
                    className={classes.button}
                    style={{
                      color: 'green',
                      width: 'fit-content',
                      marginRight: '1em',
                    }}
                    onClick={onStartProcess}
                  >
                    Wyślij do podpisu
                  </Button>
                  <Button
                    variant="outlined"
                    className={classes.button}
                    style={{ color: 'black', width: 'fit-content' }}
                    onClick={onHandelEditDocument}
                  >
                    Edytuj dane{' '}
                  </Button>
                </div>

                {/* </Link> */}
                {/* <h6>Nastepnie: Wyslij do podpisu</h6>{' '} */}
              </div>
              <Grid
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  flex: '2',
                  // marginBottom: '3em',
                }}
              >
                <span style={{ fontSize: '18px', fontWeight: '600' }}>
                  Ustawienia
                </span>
                <div style={{ display: 'flex', alignItems: 'baseline' }}>
                  <span style={{ fontWeight: '700' }} className="proces">
                    Proces:
                  </span>
                  <FormControl className={classes.formControl}>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      // value={age}
                      // onChange={handleChange}
                      defaultValue={'Podpisanie'}
                    >
                      <MenuItem value={'Podpisanie'}>Podpisanie</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div style={{ display: 'flex', alignItems: 'baseline' }}>
                  <span style={{ fontWeight: '700' }}>Podpis:</span>
                  <span
                    style={{
                      fontSize: '16px',
                      color: '#4c4949',
                      marginLeft: '6px',
                    }}
                  >
                    {!bulkDocState.selectedDocumentData.userMail
                      ? '@email'
                      : bulkDocState.selectedDocumentData.userMail}
                  </span>
                </div>
              </Grid>
            </div>
          ) : (
            <EditingDocument setEditDocument={setEditDocument} />
          )}
          {showHistory && (
            <Grid
              style={{
                paddingTop: '15px',
                paddingBottom: '15px',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              <span style={{ fontWeight: '700' }}>Historia</span>
              <div>
                <span style={{ fontWeight: '700' }}>
                  Data utworzenia dokumentu :{' '}
                </span>{' '}
                {createdDate ? preparedDate(createdDate) : ''}
              </div>
              <div>
                <span style={{ fontWeight: '700' }}>
                  Data ostatniej zmiany :
                </span>{' '}
                {lastModDate ? preparedDate(lastModDate) : ''}
              </div>
            </Grid>
          )}
          {acroFields && !editDocument && (
            <Grid
              style={{
                display: 'flex',
                alignItems: 'start',
                overflow: 'auto',
                height: '100%',
              }}
            >
              <span style={{ marginRight: '15px', fontWeight: '700' }}>
                Dane w dokumencie :
              </span>

              <Paper
                style={{
                  overflow: 'auto',
                  height: '100%',
                  maxWidth: '27em',
                  maxHeight: '26em',
                }}
              >
                <div className="data-document-block">
                  {acroFields.map((x) => {
                    if (x.type === 'TextField') {
                      return (
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                          }}
                        >
                          <div style={{ marginRight: '15px' }}>
                            <span style={{ fontWeight: '700' }}>{x.name}:</span>
                          </div>
                          <div>
                            {' '}
                            <span>{x.value}</span>
                          </div>
                        </div>
                      );
                    }

                    return <></>;
                  })}
                </div>
              </Paper>
            </Grid>
          )}
        </div>
      </Paper>

      {alertData && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={alertData}
          autoHideDuration={2000}
          onClose={() => setAlertData(null)}
        >
          <Alert
            onClose={() => setAlertData(null)}
            severity={alertData.errorType}
          >
            {alertData.message}
          </Alert>
        </Snackbar>
      )}
    </div>
  );
};
