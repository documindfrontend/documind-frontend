import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import './ConfirmationDialogWithInput.css'
export const ConfirmationDialogWithInput = ({
  title,
  open,
  handleClose,
  handleConfirm,
}) => {
  const [folderName, setFolderName] = useState('');
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      className="body-card"
      style={{display:'flex', justifyContent:"center",flexDirection:"column"}}
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <TextField 
        autoFocus
        margin="dense"
        id="name"
        label="Nowa nazwa"
        type="text"
        
        onChange={(e) => setFolderName(e.target.value)}
        style={{display:'flex', justifyContent:"center",flexDirection:"column",width:'94%', marginLeft: "10px" }}

      />
      <DialogActions style={{display:"flex", justifyContent:'center' }}>
        <Button onClick={handleClose} color="primary">
          Odrzucam
        </Button>
        <Button onClick={() => handleConfirm(folderName)} color="primary">
          Potwierdzam
        </Button>
      </DialogActions>
    </Dialog>
  );
};
