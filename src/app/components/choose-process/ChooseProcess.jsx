import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
// import FormLabel from '@material-ui/core/FormLabel';
import proceslogo from '../../../assets/process.svg';
export default function ChooseProcess() {
  const [process, setProcess] = React.useState('adding documents');

  const handleChange = (event) => {
    setProcess(event.target.value);
  };

  return (
    <FormControl component="fieldset">
      <div   style={{ display: 'flex', justifyContent:'center'}}
>
        <span>Dostępne procesy:</span></div> 
      <RadioGroup
        className="radio-group"
        style={{ display: 'flex', flexDirection: 'initial', paddingTop: '6em' }}
        aria-label="gender"
        name="gender1"
        value={process}
        onChange={handleChange}
      >
        <FormControlLabel value="adding documents" control={<Radio />} label />{' '}
        <img alt={proceslogo} src={proceslogo} />
        {/* <FormControlLabel value="male" disabled control={<Radio />} label="X" /> */}
        {/* <FormControlLabel value="other" disabled control={<Radio />} label="X" />
        <FormControlLabel value="disabled" disabled control={<Radio />} label="X" /> */}
      </RadioGroup>
    </FormControl>
  );
}
