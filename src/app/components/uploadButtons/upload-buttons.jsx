import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";



const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: "none",
  },
}));

export const UploadButtons = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>

      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span"
         startIcon={<CloudUploadIcon />}>
           Masowe dodawanie 
        </Button>
      </label>
    </div>
  );
}
