import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import SortableTree from 'react-sortable-tree';
import FileExplorerTheme from 'react-sortable-tree-theme-file-explorer';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';

import {
  selectCatalogs,
  fetchCatalogs,
  deleteCatalog,
} from 'app/store/catalogs';
import Spinner from 'app/components/spinner';
import { ConfirmationDialog } from 'app/components/confirmation-dialog/ConfirmationDialog';
import { ConfirmationDialogWithInput } from 'app/components/confirmation-dialog-with-input/ConfirmationDialogWithInput';
import horizontal from 'assets/horizontal.png';
import './Tree.scss';
import { selectBulkDocState } from 'app/store/bulk-documents-upload';

const Tree = ({ onSelectFolder, onAddNewFolder }) => {
  const dispatch = useDispatch();
  const bulkDocState = useSelector(selectBulkDocState);
  const documnetsInCurrentFolder = bulkDocState.selectedFolder.documents;
  const selectedFolder = bulkDocState.selectedFolder;
  console.log('selectedFolder', selectedFolder);

  const catalogs = useSelector(selectCatalogs);
  // const [searchString, setSearchString] = useState('');
  // const [searchFocusIndex, setsearchFocusIndex] = useState(0);

  const [treeData, setTreeData] = useState([]);
  const [dialogData, setDialogData] = useState({ show: false, data: {} });
  const [showAddNewFolderDialog, setShowAddNewFolderDialog] = useState(false);

  useEffect(() => {
    if (!catalogs.loaded) {
      dispatch(fetchCatalogs());
    } else {
      setTreeData(catalogs.treeData);
    }
  }, [catalogs, dispatch]);

  const updateTreeData = (treeData) => {
    setTreeData(treeData);
  };

  // delete Modal
  const onDeleteFolderConfirm = async () => {
    let id;
    if (dialogData.data.isDirectory) {
      id = dialogData.data.catalogId;
    } else {
      id = dialogData.data.documentId;
    }
    setDialogData({ ...dialogData, show: false });

    await dispatch(deleteCatalog(id));
    dispatch(fetchCatalogs());
  };

  const onDeleteCatalog = () => {
    setDialogData({ show: true, data: documnetsInCurrentFolder });
  };

  const onHandleNewFolderAdd = async (folderName) => {
    onAddNewFolder(folderName);
    setShowAddNewFolderDialog(false);
  };

  const customSearchMethod = ({ node, searchQuery }) => {
    return searchQuery && node.title.indexOf(searchQuery) > -1;
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
      }}
    >
      <Paper
        style={{
          height: '100%',
          width: '100%',
          overflow: 'auto',
        }}
        className="animationEffect"
      >
        {!catalogs.treeData.length ? (
          <div
            style={{
              minWidth: '20em',
              minHeight: '40em',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Spinner />
          </div>
        ) : (
          <div
            style={{
              display: 'flex',
            }}
          >
            <div
              style={{
                padding: '10px',
                border: '1px solid lightgray',
                position: 'relative',
              }}
            >
              <img
                alt="arrow-button"
                src={horizontal}
                className="arrow-button"
              />

              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <Button
                  variant="text"
                  className="button"
                  onClick={() => setShowAddNewFolderDialog(true)}
                >
                  <CreateNewFolderIcon />
                </Button>

                <Button
                  className="button"
                  disabled={!documnetsInCurrentFolder}
                  aria-label="delete"
                  onClick={() => onDeleteCatalog()}
                >
                  <DeleteIcon fontSize="small" />
                </Button>
              </div>

              <SortableTree
                style={{
                  resize: 'horizontal',
                  height: '66vh',
                  width: '200px',
                  outline: 'none',
                  overflow: 'auto',
                }}
                theme={FileExplorerTheme}
                treeData={treeData}
                onChange={updateTreeData}
                searchMethod={customSearchMethod}
                // searchQuery={searchString}
                // searchFocusOffset={searchFocusIndex}
                canDrag={false}
                canDrop={() => false}
                generateNodeProps={(rowInfo) => ({
                  icons: rowInfo.node.isDirectory
                    ? [
                        <div
                          style={{
                            borderLeft: 'solid 8px gray',
                            borderBottom: 'solid 10px gray',

                            boxSizing: 'border-box',
                            width: 16,
                            height: 12,
                            filter: rowInfo.node.expanded
                              ? 'drop-shadow(1px 0 0 gray) drop-shadow(0 1px 0 gray) drop-shadow(0 -1px 0 gray) drop-shadow(-1px 0 0 gray)'
                              : 'none',
                            borderColor:
                              rowInfo.node.catalogId !==
                              selectedFolder.catalogId
                                ? 'white'
                                : '#8c52dd',
                          }}
                        />,
                      ]
                    : [
                        <div
                          style={{
                            border: 'solid 1px black',
                            fontSize: 6,
                            textAlign: 'center',
                            marginRight: 10,
                            width: 14,
                            height: 16,
                          }}
                        >
                          pdf
                        </div>,
                      ],
                  buttons: rowInfo.node.isDirectory
                    ? [
                        <button
                          aria-label="select folder"
                          className="selectFolderButton"
                          onClick={() => {
                            onSelectFolder([rowInfo.node]);
                          }}
                        ></button>,
                      ]
                    : [],
                })}
              />
            </div>
          </div>
          // </div>
        )}
      </Paper>
      <ConfirmationDialog
        title="Czy jesteś pewny że chcesz usunąć katalog?"
        open={dialogData.show}
        handleClose={() => setDialogData({ ...dialogData, show: false })}
        handleConfirm={onDeleteFolderConfirm}
      />
      <ConfirmationDialogWithInput
        style={{ padding: '0 4em' }}
        title="Podaj nazwe nowego katalogu"
        open={showAddNewFolderDialog}
        handleClose={() => setShowAddNewFolderDialog(false)}
        handleConfirm={onHandleNewFolderAdd}
      />
    </div>
  );
};

export default Tree;
