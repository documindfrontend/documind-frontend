import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { parse } from 'papaparse';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import csv_example from '../../../assets/csv_example.csv';

import './drag-and-drop.css';

export default function DragAndDropUploader({ contacts, addContacts }) {
  const useStyles = makeStyles({
    table: {
      minWidth: 450,
    },
  });
  const classes = useStyles();

  const onDrop = useCallback(
    async (acceptedFiles) => {
      addContacts(acceptedFiles);
    },
    [addContacts]
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: (e) => onDrop(e),
    accept: '.csv,.txt',
  });

  const onCvsAded = async (event) => {
    const files = event.target.files;
    const contacts = await parseCsvToContacts(files);
    addContacts(contacts);
  };

  const parseCsvToContacts = async (files) => {
    const allowedExtentions = /(\.csv)$/i;
    return new Promise((resolve) => {
      Array.from(files)
        .filter((file) => allowedExtentions.exec(file.name))
        .map(async (file) => {
          const text = await file.text();
          try {
            const result = parse(text, { header: true, dynamicTyping: true });
            resolve(result.data);
          } catch (error) {
            console.log('error', error);
          }
        });
    });
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
      <div>
        <a className="link" href={csv_example}>
          Pobierz przykładowy plik CSV tutaj
        </a>
      </div>
      <div>
        <div
          className="drop-zone"
          {...getRootProps()}
          onDragOver={(e) => {
            e.preventDefault();
          }}
          onDrop={(e) => {
            e.preventDefault();
            parseCsvToContacts(e.dataTransfer.files);
          }}
        >
          <p style={{ color: '#ab2c2c' }}>
            Przeciągnij dokument CSV tutaj lub kliknij.{' '}
          </p>
          <input type="file" {...getInputProps()} onChange={onCvsAded} />
        </div>
      </div>
      <div>
        <ul>
          {contacts.map((contact) => (
            <TableContainer component={Paper}>
              <Table
                className={classes.table}
                size="small"
                aria-label="a dense table"
              >
                <TableHead>
                  <TableRow></TableRow>
                </TableHead>
                <TableBody>
                  <TableRow key={contact.email}>
                    <TableCell component="th" scope="row">
                      {contact.file_name}
                    </TableCell>
                    <TableCell align="right">{contact.email_address}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          ))}
        </ul>
      </div>
    </div>
  );
}
