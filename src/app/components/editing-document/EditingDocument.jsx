import { Button } from '@material-ui/core';

import React, { useState } from 'react';
// import TextField from '@material-ui/core/TextField';
// import { InputBase } from '@material-ui/core';
// import Radio from '@material-ui/core/Radio';
// import RadioGroup from '@material-ui/core/RadioGroup';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import FormControl from '@material-ui/core/FormControl';
// import FormLabel from '@material-ui/core/FormLabel';
// import Select from '@material-ui/core/Select';
// import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
// import Checkbox from '@material-ui/core/Checkbox';
import { useDispatch, useSelector } from 'react-redux';
import { selectBulkDocState } from 'app/store/bulk-documents-upload';
import { useForm } from 'react-hook-form';
import {
  updateDocumentAcroFields,
  fetchDocumentAcroFields,
} from 'app/store/bulk-documents-upload/bulk-documents-upload.thunks';

import './EditingDocument.scss';

export const EditingDocument = ({ setEditDocument }) => {
  const [alertData, setAlertData] = useState();

  const bulkDocState = useSelector(selectBulkDocState);
  // console.log('bulkDocState', bulkDocState);
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const acroFields = bulkDocState.selectedDocumentAcroFields;

  const onSubmit = async (data) => {
    const docGuid = bulkDocState.selectedDocuments[0].documentId;
    const praperedAcrofields = Object.entries(data).map(([key, value]) => ({
      name: key,
      value,
    }));
    const preparedData = {
      docGuid: docGuid,
      namesValues: praperedAcrofields,
      newFileName: 'DocumindTest',
    };
    await dispatch(updateDocumentAcroFields(preparedData));
    await dispatch(fetchDocumentAcroFields(docGuid));
    console.log('preparedData', preparedData);
    await setAlertData({
      errorType: 'success',
      message: 'Dokument został zmieniony!',
    });
    setEditDocument(false);
  };

  return (
    <div className="editing-document-block">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="editing-form-row-button">
          <Button
            type="submit"
            variant="outlined"
            className="button"
            style={{ color: 'green' }}
            // variant="outlined"
            // style={{ color: 'green' }}
            // onClick={() => setEditDocument(false)}
            // className="button"
            // selectedDocumentId={selectedDocumentId}
          >
            Zapisz dane{' '}
          </Button>
          <Button
            variant="outlined"
            className="button"
            style={{ color: 'red' }}
            onClick={() => setEditDocument(false)}

            // selectedDocumentId={selectedDocumentId}
          >
            Anuluj edycję
          </Button>
        </div>
        <div className="form-label">
          <span>Dane w dokumencie</span>
        </div>

        <div className="editing-document-block">
          {acroFields &&
            acroFields.map((x) => {
              if (x.type === 'TextField') {
                return (
                  <div className="editing-form-row">
                    <InputLabel
                      htmlFor="fieldset"
                      id="demo-customized-select-label"
                      className="label"
                    >
                      {x.name}
                    </InputLabel>
                    <input
                      ref={register}
                      defaultValue={x.value}
                      name={x.name}
                      type={x.type}
                      id="outlined-basic"
                      variant="outlined"
                      className="custom-input"
                    />
                  </div>
                );
              }

              return <></>;
              // if (x.type === 'ComboBox') {
              //   return (
              //     <div className="editing-form-row">
              //       <InputLabel
              //         htmlFor="fieldset"
              //         id="demo-customized-select-label"
              //         className="label"
              //       >
              //         {x.name}
              //       </InputLabel>
              //       <FormControl style={{ width: '100%' }}>
              //         {/* <InputLabel id="demo-customized-select-label">Age</InputLabel> */}
              //         <Select
              //           labelId="demo-customized-select-label"
              //           id="demo-customized-select"
              //           ref={register}
              //           name={x.name}
              //           type={x.type}
              //           // value={}
              //           // onChange={handleChange}
              //           // input={<BootstrapInput />}
              //         >
              //           <MenuItem value="">
              //             <em>None</em>
              //           </MenuItem>
              //           <MenuItem  ref={register}
              //           name={x.name}
              //           type={x.type} value={10}>Ten</MenuItem>
              //           <MenuItem  ref={register}
              //           name={x.name}
              //           type={x.type} value={20}>Twenty</MenuItem>
              //           <MenuItem  ref={register}
              //           name={x.name}
              //           type={x.type} value={30}>Thirty</MenuItem>
              //         </Select>
              //       </FormControl>
              //     </div>
              //   );
              // }
            })}
        </div>
      </form>
      {alertData && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={alertData}
          autoHideDuration={2000}
          onClose={() => setAlertData(null)}
        >
          <Alert
            onClose={() => setAlertData(null)}
            severity={alertData.errorType}
          >
            {alertData.message}
          </Alert>
        </Snackbar>
      )}
    </div>
  );
};
