import React, { useState, useEffect, useCallback } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import AssignmentIcon from '@material-ui/icons/Assignment';
import MenuItem from '@material-ui/core/MenuItem';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import Spinner from 'app/components/spinner';
import DeleteSweepOutlinedIcon from '@material-ui/icons/DeleteSweepOutlined';
import FingerprintOutlinedIcon from '@material-ui/icons/FingerprintOutlined';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import { deleteCatalog, postGuidsForSign } from 'app/store/catalogs';
import './SelectDocument.scss';
import { selectBulkDocState } from 'app/store/bulk-documents-upload';
import greenPointLogo from 'assets/green.png';

export const SelectDocument = () => {
  const bulkDocState = useSelector(selectBulkDocState);
  const documentId = bulkDocState.selectedDocuments[0]
    ? bulkDocState.selectedDocuments[0].documentId
    : null;
  // const [podpis, setPodpis] = useState();
  const [viewerUrl, setViewerUrl] = useState('');

  const [alertData, setAlertData] = useState();
  const dispatch = useDispatch();
  const history = useHistory();

  const getPdfUrl = useCallback(async () => {
    if (!bulkDocState.selectedDocuments[0].documentBase64) {
      return;
    }
    const blob = await fetch(
      `data:application/pdf;base64,${bulkDocState.selectedDocuments[0].documentBase64}`
    ).then((res) => res.blob());

    const url = URL.createObjectURL(blob);
    const viewerUrl =
      '/pdfjs-dist/web/viewer.html?file=' + encodeURIComponent(url);
    setViewerUrl(viewerUrl);
  }, [bulkDocState.selectedDocuments[0].documentBase64]);

  useEffect(() => {
    getPdfUrl();
  }, [getPdfUrl]);

  // -------//
  // const onOpenDocument = () => {
  //   history.push('/documents/id');
  // };
  ///////---------//////
  // const handlePodpisChange = (event) => {
  //   setPodpis(event.target.value);
  //   console.log('event.target.setPodpis', event.target.value);
  // };

  // const selectedDocument = useSelector(selectedDocument)
  const onStartProcess = () => {
    dispatch(postGuidsForSign(documentId));
    setAlertData({
      errorType: 'success',
      message: 'Sukces, proces rozpoczęty !',
    });
  };

  const onDeleteDocument = () => {
    dispatch(deleteCatalog(documentId));
  };

  const useStyles = makeStyles((theme) => ({}));
  const classes = useStyles();
  return (
    <div
      style={{
        minWidth: '20em',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flex: '2',
      }}
    >
      {!bulkDocState.selectedDocuments[0].documentBase64 ? (
        <Spinner />
      ) : (
        <Paper
          style={{
            width: '100%',
            height: '100%',
            marginLeft: '10px',
            flex: '1',
          }}
        >
          <div className="showDocument">
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                width: ' 32em ',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  padding: '8px',
                }}
              >
                <AssignmentIcon
                  style={{ marginRight: '15px', marginLeft: '10px' }}
                />

                <h2 style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}>
                  {bulkDocState.selectedDocuments[0].documentName}
                </h2>
              </div>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-start',
                  marginTop: '10px',
                  marginLeft: '10px',
                  padding: '8px',
                }}
              >
                <Button
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  style={{ marginRight: '10px' }}
                  onClick={() => history.push('documents/id')}
                >
                  <DescriptionOutlinedIcon />
                  Otwórz
                </Button>
                {/* <Link to="/documents/new-process" style={{ textDecoration: 'none' }}> */}
                <Button
                  variant="outlined"
                  className={classes.button}
                  style={{ color: 'green', marginRight: '10px' }}
                  onClick={() => onStartProcess()}
                >
                  <FingerprintOutlinedIcon />
                  Wyślij do podpisu
                </Button>
                {/* </Link> */}

                <Button
                  variant="outlined"
                  color="secondary"
                  className={classes.button}
                  onClick={() => onDeleteDocument()}
                >
                  <DeleteSweepOutlinedIcon />
                  {/* Usun */}
                </Button>
              </div>
              <Grid
                item
                xs={12}
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  // marginTop: '15px',
                  alignItems: 'center',
                  // flexWrap: 'wrap',
                }}
              >
                <div
                  className="proces"
                  style={{
                    display: 'flex',
                    alignItems: 'baseline',
                    marginLeft: '15px',
                  }}
                >
                  <p>Proces:</p>
                  <FormControl className={classes.formControl}>
                    <Select
                      style={{ marginLeft: '10px' }}
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      defaultValue={'Podpisanie'}

                      // value={age}
                      // onChange={handleChange}
                    >
                      <MenuItem value={'Podpisanie'}>Podpisanie</MenuItem>
                      {/* <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem> */}
                    </Select>
                  </FormControl>
                </div>

                <div style={{ display: 'flex', alignItems: 'baseline' }}>
                  <InputLabel htmlFor="demo-customized-textbox">
                    Podpis:
                  </InputLabel>
                  <p
                    style={{
                      fontSize: '16px',
                      color: '#2f2d2d',
                      marginLeft: '6px',
                      marginRight: '20px',
                      textOverflow: 'ellipsis',
                      width: '10em',
                      overflow: 'hidden',
                    }}
                  >
                    {!bulkDocState.selectedDocumentData.userMail
                      ? '@email'
                      : bulkDocState.selectedDocumentData.userMail}
                  </p>
                  {/* <input
                    type="text"
                    placeholder={!bulkDocState.selectedDocumentData.userMail ? '@email' : bulkDocState.selectedDocumentData.userMail}
                    onChange={handlePodpisChange}
                    className="input"
                    style={{ width: '12em', margin: '8px',fontSize: "14px" }}
                  /> */}
                </div>
              </Grid>
            </div>
            {bulkDocState.selectedDocuments[0].state === 'podpisano' && (
              <div>
                <div
                  style={{
                    display: 'flex',
                    padding: '10px 50px',
                    alignItems: 'baseline',
                  }}
                >
                  <img
                    style={{
                      width: '10px',
                      height: '10px',
                      marginRight: '5px',
                    }}
                    src={greenPointLogo}
                    alt={greenPointLogo}
                  />
                  <InputLabel>
                    {bulkDocState.selectedDocuments[0].state}{' '}
                  </InputLabel>
                </div>
              </div>
            )}
            <div style={{ display: 'flex', justifyContent: 'center' }}>
            <iframe
              title={'document'}
              src={viewerUrl}
              type="application/pdf"
              style={{ width: '50vh', height: '50vh' }}
            />
            </div>
            {alertData && (
              <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                open={alertData}
                autoHideDuration={2000}
                onClose={() => setAlertData(null)}
              >
                <Alert
                  onClose={() => setAlertData(null)}
                  severity={alertData.errorType}
                >
                  {alertData.message}
                </Alert>
              </Snackbar>
            )}
          </div>
        </Paper>
      )}
    </div>
  );
};
