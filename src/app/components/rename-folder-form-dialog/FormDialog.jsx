import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import './FormDialod.scss'

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton
        aria-label="delete"
        style={{ padding: '0px' }}
        onClick={handleClickOpen}
      >
        <CreateIcon fontSize="small" />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        // style={{width:'23em'}}
      >
        <DialogTitle id="form-dialog-title">Zmien nazwe katalogu</DialogTitle>
        <DialogContent>
          <DialogContentText>Podaj nową nazwe katalogu</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="nowa nazwa"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Odzucz
          </Button>
          <Button onClick={handleClose} color="primary">
            Zmien
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
