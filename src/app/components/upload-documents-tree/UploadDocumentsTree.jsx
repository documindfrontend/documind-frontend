import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import SortableTree, { toggleExpandedForAll } from 'react-sortable-tree';
import { ConfirmationDialog } from 'app/components/confirmation-dialog/ConfirmationDialog';
import { ConfirmationDialogWithInput } from 'app/components/confirmation-dialog-with-input/ConfirmationDialogWithInput';
import FileExplorerTheme from 'react-sortable-tree-theme-file-explorer';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Grid from '@material-ui/core/Grid';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  selectCatalogs,
  fetchCatalogs,
  deleteCatalog,
} from 'app/store/catalogs';
import './UploadDocumentsTree.scss';

export const UploadDocumentsTree = ({ onSelectFolder, onAddNewFolder }) => {
  const dispatch = useDispatch();

  const catalogs = useSelector(selectCatalogs);

  const [searchString, setSearchString] = useState('');
  const [searchFocusIndex, setsearchFocusIndex] = useState(0);
  const [searchFoundCount, setSearchFoundCount] = useState(null);
  const [treeDataForUpload, setTreeDataForUpload] = useState([]);
  const [selectedCatalogId, setSelectedCatalogId] = useState();
  const [dialogData, setDialogData] = useState({ show: false, data: {} });
  const [showAddNewFolderDialog, setShowAddNewFolderDialog] = useState(false);

  useEffect(() => {
    if (!catalogs.loaded) {
      dispatch(fetchCatalogs());
    } else {
      setTreeDataForUpload(catalogs.treeDataForUpload);
    }
  }, [catalogs, dispatch]);

  const updateTreeData = (treeDataForUpload) => {
    setTreeDataForUpload(treeDataForUpload);
  };

  const expand = (expanded) => {
    setTreeDataForUpload(
      toggleExpandedForAll({
        treeDataForUpload,
        expanded,
      })
    );
  };

  const expandAll = () => {
    expand(true);
  };

  const collapseAll = () => {
    expand(false);
  };

  // const alertNodeInfo = ({ node, path, treeIndex }) => {
  //   const objectString = Object.keys(node)
  //     .map((k) => (k === 'children' ? 'children: Array' : `${k}: '${node[k]}'`))
  //     .join(',\n   ');

  //   global.alert(
  //     'Info passed to the icon and button generators:\n\n' +
  //       `node: {\n   ${objectString}\n},\n` +
  //       `path: [${path.join(', ')}],\n` +
  //       `treeIndex: ${treeIndex}`
  //   );
  // };
  const onDeleteFolderConfirm = async () => {
    let id;
    if (dialogData.data.node.isDirectory) {
      id = dialogData.data.node.catalogId;
    } else {
      id = dialogData.data.node.documentId;
    }
    setDialogData({ ...dialogData, show: false });
    await dispatch(deleteCatalog(id));
    dispatch(fetchCatalogs());
  };

  // const onDeleteCatalog = (rowInfo) => {
  //   setDialogData({ show: true, data: rowInfo });
  // };

  const onHandleNewFolderAdd = async (folderName) => {
    onAddNewFolder(folderName);
    // await dispatch(fetchCatalogs());
    setShowAddNewFolderDialog(false);
  };
  // const onDeleteCatalog = (rowInfo) => {
  //   let id = null;
  //   if (rowInfo.node.isDirectory) {
  //     id = rowInfo.node.catalogId;
  //   } else {
  //     id = rowInfo.node.documentId;
  //   }

  //   dispatch(deleteCatalog(id));
  // };

  const onCheckFolder = (event, rowInfo) => {
    const checked = event.currentTarget.checked;
    setSelectedCatalogId(rowInfo.node.catalogId);
    onSelectFolder(checked, rowInfo.node.catalogId);
  };

  // const onCheckDocument = (event, rowInfo) => {
  //   const checked = event.currentTarget.checked;
  //   onSelectDocument(checked, rowInfo.node);
  // };

  const selectPrevMatch = () => {
    setsearchFocusIndex(
      searchFocusIndex !== null
        ? (searchFoundCount + searchFocusIndex - 1) % searchFoundCount
        : searchFoundCount - 1
    );
  };

  const selectNextMatch = () => {
    setsearchFocusIndex(
      searchFocusIndex !== null ? (searchFocusIndex + 1) % searchFoundCount : 0
    );
  };

  const onSearchFinishCallback = (matches) => {
    setSearchFoundCount(matches.length);
    setsearchFocusIndex(
      matches.length > 0 ? searchFocusIndex % matches.length : 0
    );
  };

  // const onAddNewFolder = () => {
  //   const folder = {
  //     catalogId: Math.random().toString(36).substr(2, 9),
  //     name: 'Customer folder',
  //     isDirectory: true,
  //   };
  //   dispatch(addNewCatalog(folder));
  // };
  // const onAddNewFolder = async (rowInfo) => {
  //   const folder = {
  //     rootId: 'aa6523f1-eca8-4608-8676-f96b721913a2',
  //     catalogName: `NEW FOLDER_${Math.random().toString(36).substr(2, 9)}`,
  //     // catalogId: Math.random().toString(36).substr(2, 9),
  //     // name: 'Customer folder',
  //     // isDirectory: true,
  //   };
  //   console.log("new folder:", folder)
  //   dispatch(addNewCatalog(folder));
  // };

  const customSearchMethod = ({ node, searchQuery }) =>
    searchQuery && node.title.indexOf(searchQuery) > -1;

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        width: '100%',
      }}
    >
      <Paper
        style={{
          display: 'flex',
          flexDirection: 'column',
          height: '100%',
          width: '100%',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '0 15px',
          }}
        >
          <Grid style={{ display: 'flex', alignItems: 'baseline' }}>
            <TextField
              className="search-input"
              style={{
                marginTop: '10px',
                padding: '0px',
              }}
              placeholder="Szukaj..."
              type="text"
              variant="outlined"
              id="find-box"
              value={searchString}
              onChange={(event) => {
                setSearchString(event.target.value);
              }}
            />
            <Grid style={{ display: 'flex', alignItems: 'center' }}>
              <Grid>
                <IconButton
                  size="small"
                  disabled={!searchFoundCount}
                  onClick={selectPrevMatch}
                >
                  <ArrowBackIcon fontSize="inherit" />
                </IconButton>
                <IconButton
                  size="small"
                  disabled={!searchFoundCount}
                  onClick={selectNextMatch}
                >
                  <ArrowForwardIcon fontSize="inherit" />
                </IconButton>
              </Grid>

              <Grid>
                &nbsp;
                {searchFoundCount > 0 ? searchFocusIndex + 1 : 0}
                &nbsp;/&nbsp;
                {searchFoundCount || 0}
              </Grid>
            </Grid>
          </Grid>
          <Grid style={{ display: 'flex', alignItems: 'center' }}>
            <Button variant="text" onClick={expandAll}>
              <ExpandMoreIcon />
            </Button>
            <Button variant="text" onClick={collapseAll}>
              <ExpandLessIcon />
            </Button>
            <Button
              variant="text"
              onClick={() => setShowAddNewFolderDialog(true)}
            >
              <CreateNewFolderIcon />
            </Button>
          </Grid>
        </div>

        <div
          style={{
            display: 'flex',
            height: '100%',
            border: 'solid 1px grey',
            margin: '10px',
          }}
        >
          <Grid
            style={{
              flex: '8',
              overflow: 'hidden',
              padding: '10px 10px 10px 15px',
            }}
          >
            <div style={{ height: '40px' }}>
              <h4>Nazwa</h4>
            </div>
            <SortableTree
              style={{ overflow: 'auto' }}
              theme={FileExplorerTheme}
              treeData={treeDataForUpload}
              onChange={updateTreeData}
              searchMethod={customSearchMethod}
              searchQuery={searchString}
              searchFocusOffset={searchFocusIndex}
              searchFinishCallback={onSearchFinishCallback}
              canDrag={false}
              canDrop={() => false}
              generateNodeProps={(rowInfo) => ({
                icons: rowInfo.node.isDirectory
                  ? [
                      <div
                        style={{
                          borderLeft: 'solid 8px gray',
                          borderBottom: 'solid 10px gray',
                          marginRight: 10,
                          boxSizing: 'border-box',
                          width: 16,
                          height: 12,
                          filter: rowInfo.node.expanded
                            ? 'drop-shadow(1px 0 0 gray) drop-shadow(0 1px 0 gray) drop-shadow(0 -1px 0 gray) drop-shadow(-1px 0 0 gray)'
                            : 'none',
                          borderColor: rowInfo.node.expanded ? 'white' : 'gray',
                        }}
                      />,
                      <Checkbox
                        style={{ margin: '0px', padding: '0px' }}
                        icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                        checkedIcon={<CheckBoxIcon fontSize="small" />}
                        name="checkedFolder"
                        checked={rowInfo.node.catalogId === selectedCatalogId}
                        onChange={(event) => onCheckFolder(event, rowInfo)}
                      />,
                    ]
                  : [
                      <div
                        style={{
                          border: 'solid 1px black',
                          fontSize: 6,
                          textAlign: 'center',
                          marginRight: 10,
                          width: 14,
                          height: 16,
                        }}
                      >
                        pdf
                      </div>,
                    ],
                buttons: rowInfo.node.isDirectory ? [<div></div>] : [null],
              })}
            />
          </Grid>
        </div>
      </Paper>
      <ConfirmationDialog
        title="Czy jesteś pewny że chcesz usunąć katalog?"
        open={dialogData.show}
        handleClose={() => setDialogData({ ...dialogData, show: false })}
        handleConfirm={onDeleteFolderConfirm}
      />
      <ConfirmationDialogWithInput
        title="Podaj nazwe nowego katalogu"
        open={showAddNewFolderDialog}
        handleClose={() => setShowAddNewFolderDialog(false)}
        handleConfirm={onHandleNewFolderAdd}
      />
    </div>
  );
};
