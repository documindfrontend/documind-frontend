import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';

import DialogTitle from '@material-ui/core/DialogTitle';

export const ConfirmationDialog = ({
  title,
  open,
  handleClose,
  handleConfirm,
}) => {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Odrzucam
        </Button>
        <Button onClick={handleConfirm} color="primary">
          Potwierdzam
        </Button>
      </DialogActions>
    </Dialog>
  );
};
