import React from 'react';
import { useHistory } from 'react-router';
import { useEffect } from 'react';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepConnector from '@material-ui/core/StepConnector';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import PostAddIcon from '@material-ui/icons/PostAdd';
import PlusOneIcon from '@material-ui/icons/PlusOne';
import TuneIcon from '@material-ui/icons/Tune';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import { Routes } from 'utils/routes';
import { useDidMount } from 'utils/custom-hooks';

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  completed: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
  },
});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <FolderIcon />,
    2: <PostAddIcon />,
    3: <PlusOneIcon />,
    4: <TuneIcon />,
    // 5: <SettingsIcon />,
    5: <CloudUploadIcon />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

const uploadDocumentsRoutes = [
  Routes.selectFolder,
  Routes.addDocuments,
  Routes.addCsv,
  Routes.chooseProcess,
  Routes.documentSettings,
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    'Wybierz katalog',
    'Dodaj dokumenty',
    'Dodaj plik CSV',
    'Wybierz proces',
    'Prześlij',
  ];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return `Wybierz katalog do którego chcesz dodac pliki PDF`;
    case 1:
      return 'Przeciągnij dokumenty na pole poniżej lub kliknij na pole, a następnie wybierz pliki PDF.';
    case 2:
      return `Dodaj plik CSV, dzięki któremu do każdego dokumentu zostanie przypisany adres e-mail osoby podpisującej.  `;
    case 3:
      return 'Wybierz proces.';
    case 4:
      return 'Kliknij poniżej, aby dodać dokumenty do systemu.';
    default:
      return 'Unknown step';
  }
}

export const StepperForUploadDocuments = ({ uploadWithGuids }) => {
  const history = useHistory();
  const didMount = useDidMount();
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  const steps = getSteps();
  // console.log('steps', steps);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    console.log('activeStep', activeStep);
    if (activeStep === 0) {
      console.log('Wybierz katalog do którego chcesz dodac pliki PDF');
    }
    if (activeStep === 1) {
      uploadWithGuids();
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  useEffect(() => {
    if (didMount) {
      const url = `${Routes.bulkDocumentsUpload}${uploadDocumentsRoutes[activeStep]}`;
      history.push(url);
    }
  }, [activeStep, history, didMount]);

  return (
    <div className={classes.root}>
      <Stepper
        alternativeLabel
        activeStep={activeStep}
        connector={<ColorlibConnector />}
      >
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div></div>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Wstecz
              </Button>

              {activeStep !== 4 && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className={classes.button}
                >
                  Dalej
                </Button>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
