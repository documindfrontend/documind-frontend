import React from 'react';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

const DocumentSettings = ({ onCreateNewDocument }) => {
  return (
    <div  style={{display:'flex', alignItems:'start', justifyContent:'center' }}>
      <Button
        variant="contained"
        color="primary"
        endIcon={
          <CloudUploadIcon
            style={{
              fontSize: '50px',
              marginLeft: '15px',
            }}
          ></CloudUploadIcon>
        }
        style={{ width: '20em', height: '5em', borderRadius: '35px' }}
        onClick={onCreateNewDocument}
      >
        Wyślij dokumenty
      </Button>
    </div>
  );
};

export default DocumentSettings;
