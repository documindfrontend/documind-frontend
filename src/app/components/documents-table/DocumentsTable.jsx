import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import MUIDataTable from 'mui-datatables';
import { selectBulkDocState } from 'app/store/bulk-documents-upload';

import './DocumentsTable.scss';

export const DocumentsTable = ({ onSelectDocument }) => {
  const bulkDocState = useSelector(selectBulkDocState);
  const documents = bulkDocState.selectedFolder.documents;
  const [responsive] = useState('standard');
  const [tableData, setTableData] = useState(null);
  const rowsSelected = bulkDocState.selectedDocuments.map((x) =>
    documents.findIndex((d) => d.documentId === x.documentId)
  );

  const preparedDate = (date) => {
    return date.replace(/T/g, ' ').split('.')[0];
  };

  useEffect(() => {
    if (documents) {
      const prapareData = documents
        .filter((x) => !x.isDirectory)
        .map((x) => ({
          Nazwa: x.documentName,
          Status: x.state,
          ['Data modyfikacji']: preparedDate(x.lastModified),
          data: x,
        }));
      setTableData(prapareData);
    }
  }, [documents]);

  const columns = ['Nazwa', 'Status', 'Data modyfikacji'];
  const options = {
    textLabels: {
      body: {
        noMatch: 'Przepraszamy, nie znaleziono żadnych plików',
      },
      pagination: {
        next: 'Następna strona',
        previous: 'Previous Page',
        rowsPerPage: 'Wierszy na stronie:',
        displayRows: 'of',
      },
      toolbar: {
        search: 'Szukaj',
        downloadCsv: 'Download CSV',
        print: 'Print',
        viewColumns: 'Wyświetl kolumny',
        filterTable: 'Filtruj tabelę',
      },
      filter: {
        all: 'Wszystkie',
        title: 'Filtry',
        reset: 'Resetowanie',
      },
      viewColumns: {
        title: 'Pokaż kolumny',
        titleAria: 'Show/Hide Table Columns',
      },
      selectedRows: {
        text: 'wybrany',
        delete: 'Delete',
        deleteAria: 'Delete Selected Rows',
      },
    },
    filter: true,
    filterType: 'dropdown',
    responsive,
    tableBodyHeight: '500px',
    tableBodyMaxHeight: '700px',
    download: false,
    print: false,
    rowsSelected,
    onRowSelectionChange: (currentSelected, selected, aaa) => {
      const selectedDocuments = selected.map((s) => tableData[s.index].data);
      onSelectDocument(selectedDocuments);
    },
  };

  return (
    <div style={{ width: '100%', flex: '2' }}>
      {tableData && (
        <MUIDataTable
          style={{ boxShadow: 'none', width: '100%' }}
          data={tableData}
          columns={columns}
          options={options}
        />
      )}
    </div>
  );
};
