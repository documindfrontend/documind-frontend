import React from 'react';
import './TemplatesPreviewPage.scss';
import Button from '@material-ui/core/Button';
import documentIcon from 'assets/document-icon.png'

export const TemplatesPreviewPage = () => {
  return (
    <div className="templates-preview-page">
      <div className="title-block">
      <img src={documentIcon} style={{width:'15px'}}/> <span>Umowa franczyzy</span>
      </div>
      <div className="buttons-block">
        <Button
          variant="outlined"
          style={{
            color: 'blue',
            width: 'fit-content',
            marginRight: '1em',
          }}
        >
          Masowe tworzenie
        </Button>
        <Button
          variant="outlined"
          style={{
            color: 'green',
            width: 'fit-content',
            marginRight: '1em',
          }}
        >
          Nowy z wzoru
        </Button>
        <Button
          variant="outlined"
          style={{
            color: 'black',
            width: 'fit-content',
            marginRight: '1em',
          }}
        >
          Edytuj pola
        </Button>
        <Button
          variant="outlined"
          style={{
            color: 'black',
            width: 'fit-content',
            marginRight: '1em',
          }}
        >
          Stworz formularz WWW
        </Button>
        <Button
          variant="outlined"
          style={{
            color: 'red',
            width: 'fit-content',
            marginRight: '1em',
          }}
        >
          Usun
        </Button>
      </div>
      <div className="settings-block">
        <div className="settings-item">
          <span>Domyslny proces:</span>
          <input></input>
        </div>
        <div className="settings-item">
          <span>Domysline tagi:</span>
          <input></input>
        </div>
      </div>
      <div className="iframe-block">
        <iframe
          title={'document'}
          src={
            'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf'
          }
          type="application/pdf"
          style={{ width: '100%', height: '100%', borderRadius: '5px' }}
        />
      </div>
    </div>
  );
};
