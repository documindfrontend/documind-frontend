import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';

import './DragAndDropUploaderForOne.css';

export const DragAndDropUploaderForOne = ({ pdfFilesUploaded }) => {
  const onDrop = useCallback(
    async (acceptedFiles) => {
      pdfFilesUploaded(acceptedFiles);
    },
    [pdfFilesUploaded]
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: (e) => onDrop(e),
    accept: '.pdf',
    multiple: false,
  });

  return (
    <section>
      <div>
        <div className="drop-zone" {...getRootProps()}>
          <input type="file" {...getInputProps()} />
          <p style={{ color: '#ab2c2c' }}>
            Przeciągnij dokument PDF tutaj lub kliknij.
          </p>
        </div>
      </div>
    </section>
  );
};
