import React, { useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import { useDropzone } from 'react-dropzone';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';

export default function DragAndDropUploader({ files, pdfFilesUploaded }) {
  const useStyles = makeStyles({
    table: {
      minWidth: 450,
      // minHeight:450
    },
  });

  const classes = useStyles();

  const onDrop = useCallback(
    async (acceptedFiles) => {
      pdfFilesUploaded(acceptedFiles);
    },
    [pdfFilesUploaded]
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: (e) => onDrop(e),
    accept: '.pdf',
  });

  return (
    <section
      style={{ display: 'flex', flexDirection: 'column', width: '100%' }}
    >
      <div>
        <div className="drop-zone" {...getRootProps()}>
          <input {...getInputProps()} />
          <p style={{ color: '#ab2c2c' }}>
            Przeciągnij dokumenty tutaj lub kliknij.
          </p>
        </div>
      </div>
      <aside>
        {files.map((f) => (
          <TableContainer component={Paper}>
            <Table
              className={classes.table}
              size="small"
              aria-label="a dense table"
            >
              <TableBody>
                <TableRow key={Math.random().toString(36).substr(2, 9)}>
                  <TableCell component="th" scope="row">
                    {f.name}
                  </TableCell>
                  <TableCell align="right">Loaded</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        ))}
      </aside>
    </section>
  );
}
