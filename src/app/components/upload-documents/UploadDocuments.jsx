import React, { Fragment, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Routes } from 'utils/routes';

import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

import Button from '@material-ui/core/Button';
import backIcon from 'assets/backIcon.png';
import { StepperForUploadDocuments } from 'app/components/stepper-for-upload-documents/StepperForUploadDocuments';
import DragAndDropUploader from 'app/components/drag-and-drop-uploader/DragAndDropUploader';
import DragAndDropUploaderCsv from 'app/components/drag-and-drop-uploader-csv/DragAndDropUploaderCsv';
import { UploadDocumentsTree } from 'app/components/upload-documents-tree/UploadDocumentsTree';
import ChooseProcess from 'app/components/choose-process/ChooseProcess';
import DocumentSettings from 'app/components/document-settings/DocumentSettings';
import { selectBulkDocState } from 'app/store/bulk-documents-upload';
import { fetchCatalogs } from 'app/store/catalogs';
import {
  uploadFiles,
  getFilesGuids,
  createNewDocument,
  addNewCatalog,
  resetUploadedFiles,
} from 'app/store/bulk-documents-upload';

export const UploadDocuments = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const { uploadedFiles } = useSelector(selectBulkDocState);

  const [files, setFiles] = useState([]);
  const [contacts, setContacts] = React.useState([]);
  const [selectedFolders, setSelectedFolders] = useState([]);
  const [alertData, setAlertData] = useState();

  const getNewUniqueFiles = (newFiles) => {
    if (!files || !files.length) {
      return newFiles;
    }
    // const uniqueFileNames = files.map((file) => file.name);
    // const uniqueFiles = newFiles.filter(
    //   (file) => !uniqueFileNames.includes(file.name)
    // );
    const uniqueFiles = newFiles.filter((x) => {
      return !files.find((y) => y.name === x.name);
    });

    return uniqueFiles;
  };

  const onUploadWithGuids = async () => {
    const filesToUpload = [];
    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      element.id = uploadedFiles[index].guid;
      element.uploaded = uploadedFiles[index].uploaded;
      filesToUpload.push(element);
    }
    const notUploadedFiles = filesToUpload.filter((x) => !x.uploaded);
    await dispatch(uploadFiles(notUploadedFiles));
  };

  const onCreateNewDocument = async () => {
    files.forEach(async (file, index) => {
      const document = {
        catalogId: selectedFolders[0],
        fileGuid: uploadedFiles[index].guid,
        assignedUsers: contacts
          .filter((c) => c.file_name === file.name)
          .map((c) => c.email_address),
        documentState: 'przygotowano',
        organisationId: 'test Organization ',
        whoCreated: 'test User',
      };
      await dispatch(createNewDocument(document));
      await dispatch(resetUploadedFiles());
      await dispatch(fetchCatalogs());
    });
    setAlertData({
      errorType: 'success',
      message: 'Dokumenty zostały dodane. Znajdź je w zakładce Dokumenty!',
    });
    setTimeout(() => {
      history.push('/documents');
    }, 2500);
  };

  const onAddNewFolder = async (catalogName) => {
    if (!selectedFolders.length) {
      setAlertData({
        errorType: 'error',
        message: 'Nowy folder" Nie był stworzony!',
      });
      return;
    }
    const rootId = selectedFolders[0];
    const folder = {
      rootId,
      catalogName,
    };
    await dispatch(addNewCatalog(folder));
    await dispatch(fetchCatalogs());
    setAlertData({
      errorType: 'success',
      message: 'Nowy folder" był stworzony!',
    });
  };

  const onSelectFolder = (checked, selectedFile) => {
    if (checked) {
      setSelectedFolders([selectedFile]);
    } else {
      const documents = selectedFolders.filter(
        // (x) => x.catalogId !== selectedFile.catalogId
        (x) => x.catalogId !== selectedFile
      );
      setSelectedFolders(documents);
    }
  };

  const onPdfFilesUploaded = (newFiles) => {
    const uniqueFiles = getNewUniqueFiles(newFiles);
    setFiles([...files, ...uniqueFiles]);
    dispatch(getFilesGuids(uniqueFiles.length));
  };

  const onAddContacts = (newContacts) => {
    setContacts([...contacts, ...newContacts]);
  };
  const onHandelBackClick = () => {
    // history.goBack();
    history.push(Routes.documentsPage);
  };
  return (
    <Grid item xs={12} style={{ display: 'flex', flexDirection: 'column' }}>
      <Grid item xs={12}>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            paddingBottom: '10px',
          }}
        >
          <Button
            variant="outlined"
            color="default"
            className="button"
            onClick={() => onHandelBackClick()}
          >
            <img
              src={backIcon}
              alt="backIcon"
              style={{ width: '15px', marginRight: '5px' }}
            />
            Powrót
          </Button>
          <span
            style={{ fontSize: '25px', paddingLeft: '50px' }}
            className="page-title"
          >
            Masowe dodawanie
          </span>
        </div>

        <StepperForUploadDocuments uploadWithGuids={onUploadWithGuids} />
      </Grid>

      <Grid
        item
        style={{
          display: 'flex',
          justifyContent: 'center',
          marginTop: '20px',
          height: '37em',
          width: '100%',
          marginLeft: '0px',
        }}
        xs={12}
      >
        <Switch style={{ display: 'flex' }}>
          <Route
            path={`${Routes.bulkDocumentsUpload}${Routes.selectFolder}`}
            render={() => (
              <UploadDocumentsTree
                onSelectFolder={onSelectFolder}
                onAddNewFolder={onAddNewFolder}
              />
            )}
            exact
          />
          <Route
            path={`${Routes.bulkDocumentsUpload}${Routes.addDocuments}`}
            component={() => (
              <DragAndDropUploader
                pdfFilesUploaded={onPdfFilesUploaded}
                files={files}
              />
            )}
            exact
          />

          <Route
            path={`${Routes.bulkDocumentsUpload}${Routes.addCsv}`}
            render={() => (
              <DragAndDropUploaderCsv
                contacts={contacts}
                addContacts={onAddContacts}
              />
            )}
            exact
          />
          <Route
            path={`${Routes.bulkDocumentsUpload}${Routes.chooseProcess}`}
            component={ChooseProcess}
            exact
          />
          <Route
            path={`${Routes.bulkDocumentsUpload}${Routes.documentSettings}`}
            component={() => (
              <DocumentSettings onCreateNewDocument={onCreateNewDocument} />
            )}
            exact
          />
        </Switch>
      </Grid>
      {alertData && (
        <Fragment>
          <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            open={alertData}
            autoHideDuration={2000}
            onClose={() => setAlertData(null)}
          >
            <Alert
              onClose={() => setAlertData(null)}
              severity={alertData.errorType}
            >
              {alertData.message}
            </Alert>
          </Snackbar>
        </Fragment>
      )}
    </Grid>
  );
};
