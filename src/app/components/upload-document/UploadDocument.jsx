import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import AssignmentIcon from '@material-ui/icons/Assignment';
import InputLabel from '@material-ui/core/InputLabel';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import backIcon from 'assets/backIcon.png';
import MenuItem from '@material-ui/core/MenuItem';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import Select from '@material-ui/core/Select';
import { useDispatch, useSelector } from 'react-redux';
import { Routes } from 'utils/routes';

import { DragAndDropUploaderForOne } from 'app/components/drag-and-drop-uploader-for-one/DragAndDropUploaderForOne';
import './UploadDocument.scss';
import { selectBulkDocState } from 'app/store/bulk-documents-upload';

import {
  uploadFiles,
  getFilesGuids,
  createNewDocument,
} from 'app/store/bulk-documents-upload';

import { fetchCatalogs } from 'app/store/catalogs';

export const UploadDocument = () => {
  const history = useHistory();
  // const bulkDocState = useSelector(selectBulkDocState);
  const { uploadedFiles, selectedFolder } = useSelector(selectBulkDocState);

  const dispatch = useDispatch();
  const [files, setFiles] = useState([]);
  // const [age, setAge] = React.useState('');
  const [oneFileBase64, setOneFileBase64] = useState();
  const [alertData, setAlertData] = useState();
  const [fileName, setFileName] = useState([]);
  const [podpis, setPodpis] = useState('');
  const [proces, setProces] = useState('');
  const [creater] = useState('');

  const BootstrapInput = withStyles((theme) => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 26px 10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      overflow: 'hidden',
      padding: theme.spacing(0, 3),
      marginTop: '10px',
      display: 'flex',
    },
    documentView: {
      height: '62vh',
      justifyContent: 'center',
      display: 'flex',
      marginRight: '25px',
      marginLeft: '25px',
      marginTop: '20px',
      boxShadow:
        '4px 6px 5px -1px rgba(0,0,0,0.2), 0px 1px 33px 0px rgba(0,0,0,0.14), 0px 1px 16px 0px rgba(0,0,0,0.12)',
    },
    button: {
      // height: '30px',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));
  const classes = useStyles();

  const onPdfFilesUploaded = (files) => {
    setFileName(files[0].name);
    setFiles([...files]);
    dispatch(getFilesGuids(files.length));
    const reader = new FileReader();
    reader.onload = (e) => {
      setOneFileBase64(e.target.result);
    };
    reader.readAsDataURL(files[0]);
  };

  // const handleChange = (event) => {
  //   setAge(event.target.value);
  // };

  const onCreateNewDocument = async () => {
    const file = files[0];
    file.id = uploadedFiles[uploadedFiles.length - 1].guid;
    await dispatch(uploadFiles([file]));

    const document = {
      catalogId: selectedFolder.catalogId,
      fileGuid: uploadedFiles[uploadedFiles.length - 1].guid,
      assignedUsers: [podpis],
      documentState: 'przygotowano',
      organisationId: 'Documind',
      whoCreated: creater,
    };
    await dispatch(createNewDocument(document));
    await dispatch(fetchCatalogs());
    setAlertData({
      errorType: 'success',
      message: 'Dokumenty zostały dodane. Znajdź je w zakładce Dokumenty!',
    });
    setTimeout(() => {
      history.push('/documents');
    }, 2500);
  };
  // const fileSelectHandler = (event) => {
  //   console.log(event.target.files[0]);
  // };

  const handlePodpisChange = (event) => {
    setPodpis(event.target.value);
    // console.log('event.target.setPodpis', event.target.value);
  };
  const handleProcesChange = (event) => {
    setProces(event.target.value);
    // console.log('event.target.setProces', event.target.value);
  };

  const onHandelBackClick = () => {
    const url = `${Routes.documentsPage}`;
    history.push(url);
  };
  // -----------
  return (
    <Grid item xs={12} className="root">
      <Grid item xs={12}>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            paddingBottom: '10px',
          }}
        >
          <Button
            variant="outlined"
            color="default"
            className="button"
            onClick={() => onHandelBackClick()}
          >
            <img
              src={backIcon}
              alt="backIcon"
              style={{ width: '15px', marginRight: '5px' }}
            />
            Powrót
          </Button>
          <span
            style={{ fontSize: '25px', paddingLeft: '50px' }}
            className="page-title"
          >
            Dodawanie dokumenta
          </span>
        </div>

        <DragAndDropUploaderForOne
          pdfFilesUploaded={onPdfFilesUploaded}
          file={files}
        />
      </Grid>
      <Paper style={{ display: 'flex', height: '67vh' }}>
        <Grid
          item
          xs={12}
          style={{ display: 'flex', justifyContent: 'center' }}
        >
          <Card
            style={{ marginBottom: '2em' }}
            className={classes.documentView}
          >
            <iframe
              title="iframe-file"
              src={oneFileBase64}
              type="application/pdf"
              style={{ width: '66vh', height: '62vh' }}
            />
          </Card>
        </Grid>
        <Grid item xs={12} style={{ display: 'flex', flexDirection: 'column' }}>
          <Grid
            item
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: '1em',
              marginTop: '3em',
              marginBottom: '3em',
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <AssignmentIcon style={{ marginRight: '10px' }} />
              <h3>{fileName}</h3>
            </div>
          </Grid>

          <Grid
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              height: '12em',
              marginBottom: '3em',
            }}
          >
            <h4 style={{ marginBottom: '0 auto' }}>Ustawienia</h4>{' '}
            <div style={{ display: 'flex', alignItems: 'baseline' }}>
              <InputLabel htmlFor="demo-customized-textbox">Proces:</InputLabel>
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                value={proces}
                onChange={handleProcesChange}
                input={<BootstrapInput />}
                style={{ width: '13em', margin: '8px' }}
              >
                <MenuItem value={'Podpisanie'}>Podpisanie</MenuItem>
              </Select>
            </div>
            <div style={{ display: 'flex', alignItems: 'baseline' }}>
              <InputLabel htmlFor="demo-customized-textbox">Podpis:</InputLabel>
              <input
                type="text"
                value={podpis}
                placeholder="@email"
                onChange={handlePodpisChange}
                className="input"
                style={{ width: '13em', margin: '8px' }}
              />
            </div>
            <Button
              variant="outlined"
              className={classes.button}
              style={{
                color: 'green',
                width: 'fit-content',
                marginTop: '4em',
              }}
              onClick={onCreateNewDocument}
            >
              Dodac dokument
            </Button>
          </Grid>
        </Grid>
      </Paper>

      {alertData && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={alertData}
          autoHideDuration={2000}
          onClose={() => setAlertData(null)}
        >
          <Alert
            onClose={() => setAlertData(null)}
            severity={alertData.errorType}
          >
            {alertData.message}
          </Alert>
        </Snackbar>
      )}
    </Grid>
  );
};
