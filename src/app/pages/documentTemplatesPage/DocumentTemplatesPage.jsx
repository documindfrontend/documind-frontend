import React, { useState } from 'react';
import './DocumentTemplatesPage.scss';
import docLogo from 'assets/doc_logo.png';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { TemplatesPreviewPage } from 'app/components/templates-preview-page/TemplatesPreviewPage';
import TextField from '@material-ui/core/TextField';
import documentIcon from 'assets/document-icon.png';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
export const DocumentTemplatesPage = () => {
  const [visible, setVisible] = useState(true);

  const hideComponent = () => {
    setVisible(!visible);
  };

  return (
    <div className="templates">
      <div className="starting-static-block">
        <div className="row-block">
          <div>
            <span style={{ fontSize: '18px', marginRight: '20px' }}>
              Tworzenie wzorow
            </span>
            <Button
              variant="outlined"
              style={{
                color: 'green',
                width: 'fit-content',
                marginRight: '1em',
              }}
            >
              Nowy wzor
            </Button>
          </div>
          {visible ? (
            <VisibilityIcon onClick={hideComponent} className="visibleIcon" />
          ) : (
            <VisibilityOffIcon
              onClick={hideComponent}
              className="visibleIcon"
            />
          )}
        </div>
        <div className="title-block">
          <span className="title">Wzory dokumentow</span>

          <TextField
            className="search-input"
            placeholder="Szukaj..."
            type="text"
            variant="outlined"
            id="find-box"
      
          />
        </div>
      </div>
      <div className="row">
        <div className="templates-block">
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy 2</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy 4</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" /> <span> test</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy document</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy 33</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy !</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy</span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzyUmowaTest Umowa Test </span>
            </div>
          </div>
          <div className="templates-item">
            <div>
              <img src={docLogo} alt="doc_logo" />
            </div>
            <div className="templates-item-name">
              <img src={documentIcon} alt="doc_logo" />{' '}
              <span> Umowa fraczyzy test</span>
            </div>
          </div>
        </div>
        {visible && (
          <Paper className="document-preview-page">
            <TemplatesPreviewPage />
          </Paper>
        )}
      </div>
    </div>
  );
};
