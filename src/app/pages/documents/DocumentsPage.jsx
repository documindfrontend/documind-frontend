import React, { Fragment, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import Tooltip from '@material-ui/core/Tooltip';
import FingerprintOutlinedIcon from '@material-ui/icons/FingerprintOutlined';
import Button from '@material-ui/core/Button';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import {UploadButtons} from 'app/components/uploadButtons/upload-buttons';
import { SelectDocument } from 'app/components/select-document/SelectDocument';
import Tree from 'app/components/tree-drag/Tree';
import { fetchCatalogs, sendGuidsForSignForMany } from 'app/store/catalogs';
import {
  addNewCatalog,
  fetchDocumentContent,
  fetchDocument,
  fetchDocumentAcroFields,
  selectBulkDocState,
} from 'app/store/bulk-documents-upload';
import {
  setSelectedDocuments,
  setSelectedFolder,
} from 'app/store/bulk-documents-upload';
import { DocumentsTable } from 'app/components/documents-table/DocumentsTable';
import './DocumentsPage.scss';

export const DocumentsPage = () => {
  const dispatch = useDispatch();

  const bulkDocState = useSelector(selectBulkDocState);
  console.log('bulkDocState ! test ',bulkDocState)
  const [alertData, setAlertData] = useState();
  const [visible, setVisible] = useState(true);

  const [selectedFolders, setSelectedFolders] = useState([]);

  const hideComponent = () => {
    setVisible(!visible);
  };

  const onStartProcessWithManyDoc = () => {
    const ids = bulkDocState.selectedDocuments.map(
      (document) => document.documentId
    );

    dispatch(sendGuidsForSignForMany(ids));
    setAlertData({
      errorType: 'success',
      message: 'Sukces, proces rozpoczęty!',
    });
  };

  const onSelectDocument = (documents) => {
    if (!bulkDocState.selectedDocuments.length) {
      dispatch(fetchDocumentContent(documents[0].documentId));
      dispatch(fetchDocument(documents[0].documentId));
      dispatch(fetchDocumentAcroFields(documents[0].documentId));
    }
    dispatch(setSelectedDocuments(documents));
  };

  const onSelectFolder = (selectedFolders) => {
    setSelectedFolders(selectedFolders);
    dispatch(setSelectedFolder(selectedFolders[0]));
  };

  const onAddNewFolder = async (catalogName) => {
    if (!selectedFolders.length) {
      setAlertData({
        errorType: 'error',
        message:
          'Folder nie dodany ! Na początek musisz wybrac folder do którego chcesz dodac!',
      });
      // setOpen(true);
      return;
    }
    const rootId = selectedFolders[0].catalogId;
    const folder = {
      rootId,
      catalogName,
    };
    await dispatch(addNewCatalog(folder));
    await dispatch(fetchCatalogs());
    setAlertData({
      errorType: 'success',
      message: 'Nowy folder" był stworzony!',
    });
  };

  return (
    <Fragment>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <span style={{ fontSize: '25px' }} className="page-title">
          Dokumenty
        </span>
      </div>

      <div className="row"  >
        <div className="buttonBar">
          <div style={{ marginRight: '40px' }}>
            <h4>Tworzenie dokumentów</h4>
          </div>
          <div className="buttons-row">
            {bulkDocState.selectedFolder.catalogId ? (
              <Link
                to="/documents/document-upload"
                // style={{ }}
                className="link"
              >
                <Button
                  // variant="disabled"
                  color="default"
                  className="button"
                  startIcon={<AddCircleOutlineIcon />}
                >
                  Nowy dokument
                </Button>
              </Link>
            ) : (
              <Tooltip
                // style={{ fontSize: '25px' }}
                title={
                  <span style={{ fontSize: '15px' }}>
                    "Najpierw muszesz wybrac folder !"
                  </span>
                }
                aria-label="Najpierw muszezs wybrac folder, do ktorego chcesz dodac 'Nowy dokument' !"
              >
                <span>
                  <Button
                    disabled={true}
                    color="default"
                    className="button"
                    startIcon={<AddCircleOutlineIcon />}
                  >
                    Nowy dokument
                  </Button>
                </span>
              </Tooltip>
            )}

            <Button
              // variant="contained"
              disabled={true}
              color="default"
              className="button"
              startIcon={<NoteAddIcon />}
              style={{ marginLeft: '7px' }}
            >
              Nowy z wzoru
            </Button>
            <Link
              to="/documents/bulk-documents-upload/select-folder"
              className="link"
            >
              <UploadButtons />
            </Link>
          </div>
          <div className="eye">
            {visible ? (
              <VisibilityIcon onClick={hideComponent} className="visibleIcon" />
            ) : (
              <VisibilityOffIcon
                onClick={hideComponent}
                className="visibleIcon"
              />
            )}
          </div>
        </div>
      </div>
      <div className="gridContainer">
        <Tree onSelectFolder={onSelectFolder} onAddNewFolder={onAddNewFolder} />
        {bulkDocState.selectedFolder.documents && (
          <DocumentsTable onSelectDocument={onSelectDocument} />
        )}

        {visible &&
          bulkDocState.selectedDocuments[0] &&
          bulkDocState.selectedDocuments[0].documentId && (
            <>
              {bulkDocState.selectedDocuments.length > 1 ? (
                <div
                  style={{
                    display: 'flex',
                    flex: 1,
                    // flexShrink:'2',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Button
                    variant="outlined"
                    className="button"
                    style={{ color: 'green', margin: '10em' }}
                    onClick={onStartProcessWithManyDoc}
                  >
                    <FingerprintOutlinedIcon />
                    Wyślij do podpisu
                  </Button>
                </div>
              ) : (
                <SelectDocument />
              )}
            </>
          )}
      </div>

      {alertData && (
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={alertData}
          autoHideDuration={2000}
          onClose={() => setAlertData(null)}
        >
          <Alert
            onClose={() => setAlertData(null)}
            severity={alertData.errorType}
          >
            {alertData.message}
          </Alert>
        </Snackbar>
      )}
    </Fragment>
  );
};
