import React, { useState } from 'react';
import MUIDataTable from 'mui-datatables';
import { Button } from '@material-ui/core';
import './RecipientBook.scss';

export const RecipientBook = () => {
  const [responsive] = useState('standard');
  // const [tableData, setTableData] = useState([]);

  //   const data = [
  //     {
  //         documentName: 'x.documentName',
  //         state: 'x.state',
  //     },
  //     {
  //         documentName: 'x.ddddocumentName',
  //         state: 'x.aaastate',
  //       },
  //   ];
  // //   setTableData(prapareData);
  //   useEffect(() => {
  //     //   if (selectedFolders[0]) {
  //        const prapareData = data.map((x) => ({
  //             Nazwa: x.documentName,
  //             Status: x.state,
  //             // data: x,
  //           }));
  //         setTableData(prapareData);
  //     //   }
  //     }, []);

  const columns = ['Imię', 'Nazwisko', 'Ulica', 'Numer domu'];
  const options = {
    textLabels: {
      body: {
        noMatch: 'Przepraszamy, nie znaleziono żadnych plików',
      },
      pagination: {
        next: 'Następna strona',
        previous: 'Previous Page',
        rowsPerPage: 'Wierszy na stronie:',
        displayRows: 'of',
      },
      toolbar: {
        search: 'Szukaj',
        downloadCsv: 'Download CSV',
        print: 'Print',
        viewColumns: 'Wyświetl kolumny',
        filterTable: 'Filtruj tabelę',
      },
      filter: {
        all: 'Wszystkie',
        title: 'Filtry',
        reset: 'Resetowanie',
      },
      viewColumns: {
        title: 'Pokaż kolumny',
        titleAria: 'Show/Hide Table Columns',
      },
      selectedRows: {
        text: 'rząd wybrany',
        delete: 'Delete',
        deleteAria: 'Delete Selected Rows',
      },
    },
    filter: true,
    filterType: 'dropdown',
    responsive,
    tableBodyHeight: '500px',
    tableBodyMaxHeight: '700px',
    download: false,
    print: false,
    // onRowSelectionChange: (currentSelected, selected) => {
    //   const selectedDocuments = selected.map((s) => tableData[s.index].data);
    //   onSelectDocument(selectedDocuments);
    // },
  };

  return (
    <div className="book-block">
      <div className="row">
        <span className="title">Książka adresowa</span>

        <Button
          variant="outlined"
          style={{ color: 'green', marginLeft: '1em' }}
        >
          Nowy kontakt
        </Button>
        <Button variant="outlined" style={{ color: 'blue', marginLeft: '1em' }}>
          Importuj kontakty
        </Button>
      </div>
      <MUIDataTable
        style={{ boxShadow: 'none' }}
        // title={selectedFolders[0].title}
        // data={tableData}
        columns={columns}
        options={options}
      />
    </div>
  );
};
