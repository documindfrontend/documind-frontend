import React, { useEffect, useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, Route, useHistory } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import clsx from 'clsx';

import { useTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';

import AssignmentIcon from '@material-ui/icons/Assignment';
import FolderIcon from '@material-ui/icons/Folder';
import DeviceHubIcon from '@material-ui/icons/DeviceHub';

import ArchiveIcon from '@material-ui/icons/Archive';
import HomeIcon from '@material-ui/icons/Home';
import ComputerIcon from '@material-ui/icons/Computer';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { AppBar, Toolbar } from '@material-ui/core';
import { Menu as MenuIcon } from '@material-ui/icons';

import { NavBarMenu } from 'app/components/navbar-menu/navbar-menu';
import { DocumentsPage } from 'app/pages/documents/DocumentsPage';
import NewProcess from 'app/components/new-process/NewProcess';
import { OpenedDocument } from 'app/components/opened-document/OpenedDocument';

import { UploadDocuments } from 'app/components/upload-documents/UploadDocuments';
import { UploadDocument } from 'app/components/upload-document/UploadDocument';
import AuthService from 'app/services/authService';
import { selectIsLoggedIn } from 'app/store/account';

import logo from 'assets/Screenshot_ic.png';
import './app.css';
import {DocumentTemplatesPage} from './pages/documentTemplatesPage/DocumentTemplatesPage';
import { RecipientBook } from './pages/recipientBook/RecipientBook';

const drawerWidth = 250;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 0,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const App = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const [user, setUser] = useState(null);
  const account = useSelector(selectIsLoggedIn);
  console.log(account);
  const history = useHistory();

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const getUser = useCallback(async () => {
    const authService = new AuthService();
    const user = await authService.getUser();
    if (!user) {
      authService.initialize();
    } else {
      history.push('/documents');
      sessionStorage.setItem('access_token', user.access_token);
      setUser(user);
    }
  }, [history]);

  useEffect(() => {
    getUser();
  }, [getUser]);

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* <NavBar /> */}
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar className="header-bar" style={{ background: '#182b4d' }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <NavBarMenu />
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <img alt={logo} src={logo} style={{ width: '155px' }} />
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>

        <Divider />
        <List key={Math.random().toString(36).substr(2, 9)}>
          {[
            {
              text: 'Domowa',

              // variant="disabled",
              icon: <HomeIcon />,
              // link: 'home'
            },
          ].map((item, index) => (
            <Link
              key={Math.random().toString(36).substr(2, 9)}
              to={item.link || '/documents'}
              style={{ textDecoration: 'none' }}
            >
              <ListItem button key={Math.random().toString(36).substr(2, 9)}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText
                  primary={item.text}
                  style={{ color: 'rgba(0, 0, 0, 0.99)' }}
                />
              </ListItem>
            </Link>
          ))}
        </List>
        <Divider />
        <List>
          {[
            {
              text: 'Moja praca',
              icon: <MailIcon />,
              // link: 'mywork'
            },
            {
              text: 'Wzory dokumentow',
              icon: (
                <AssignmentIcon
                  style={{ color: 'rgb(195 72 72)', fontWeight: '600' }}
                />
              ),
              link: '/templates',
            },
            {
              text: 'Dokumenty',
              icon: (
                <FolderIcon
                  style={{ color: 'rgb(195 72 72)', fontWeight: '600' }}
                />
              ),
              link: '/documents',
            },
            {
              text: 'Procesy',
              icon: <DeviceHubIcon />,
              // link: '/procesy',
            },
            // {
            //   text: 'Kreator dokuntow-beta ',
            //   icon: <CachedIcon />,
            //   link: '/kreator',
            // },
            // {
            //   text: 'Insights-beta',
            //   icon: <TrendingUpIcon />,
            //   link: '/insights',
            //   badge: <FiberNewIcon style={{ color: '#d32f2f' }} />,
            // },
            {
              text: 'Książka odbiorców ',
              icon: (
                <ArchiveIcon
                  style={{ color: 'rgb(195 72 72)', fontWeight: '600' }}
                />
              ),
              link: '/recipients',
            },
          ].map((item, index) => (
            <Link
              key={Math.random().toString(36).substr(2, 9)}
              to={item.link || '/documents'}
              style={{ textDecoration: 'none' }}
            >
              <ListItem button key={index}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText
                  primary={item.text}
                  style={{ color: 'rgba(0, 0, 0, 0.54)' }}
                />
                <ListItemIcon>{item.badge}</ListItemIcon>
              </ListItem>
            </Link>
          ))}
        </List>
        <Divider />
        <List>
          {[
            {
              text: 'Programisci',
              icon: <ComputerIcon />,
              // link: 'programmers',
            },
            {
              text: 'Wsparcie',
              icon: <QuestionAnswerIcon />,
              // link: 'support'
            },
            {
              text: 'Administracja',
              icon: <SupervisorAccountIcon />,
              // link: 'administration',
            },
          ].map((item, index) => (
            <Link
              key={Math.random().toString(36).substr(2, 9)}
              to={item.link || '/documents'}
              style={{ textDecoration: 'none' }}
            >
              <ListItem button key={index}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText
                  primary={item.text}
                  style={{ color: 'rgba(0, 0, 0, 0.54)' }}
                />
              </ListItem>
            </Link>
          ))}
        </List>

        <Divider />
        {/* <List style={{ textDecoration: 'none' }}>
          <ListItem button onClick={handleClick}>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Inboxxx" />
            {openTest ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={false} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button className={classes.nested}>
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Starred" />
              </ListItem>
              <ListItem button className={classes.nested}>
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Starred" />
              </ListItem>
            </List>
          </Collapse>
        </List> */}
      </Drawer>

      <main className={classes.content} style={{ height: '100vh' }}>
        <div className={classes.toolbar} />
        {user && (
          <Switch>
            <Route
              key="/documents"
              path="/documents"
              component={DocumentsPage}
              exact
            />
            <Route
              key={Math.random().toString(22).substr(2, 9)}
              path="/documents/new-process"
              component={NewProcess}
              exact
            />
            <Route
              key={Math.random().toString(13).substr(2, 9)}
              path="/documents/id"
              component={OpenedDocument}
              exact
            />
            <Route
              key={Math.random().toString(14).substr(2, 9)}
              path="/documents/id/new-process"
              component={NewProcess}
              exact
            />
            <Route
              key={Math.random().toString(14).substr(2, 9)}
              path="/documents/document-upload"
              component={UploadDocument}
              exact
            />
            <Route
              key={Math.random().toString(14).substr(2, 9)}
              path="/documents/bulk-documents-upload/:id"
              component={UploadDocuments}
              exact
            />
              <Route
              key={Math.random().toString(14).substr(2, 9)}
              path="/templates"
              component={DocumentTemplatesPage}
              exact
            />
              <Route
              key={Math.random().toString(14).substr(2, 9)}
              path="/recipients"
              component={RecipientBook}
              exact
            />
            {/* <Redirect to="/documents" exact /> */}
          </Switch>
        )}
      </main>
    </div>
  );
};

export default App;
