import { createSlice } from '@reduxjs/toolkit';
import { catalogsAdapter } from './catalogs.adapter';

import { fetchCatalogs } from './catalogs.thunks';

export const catalogsSlice = createSlice({
  name: '[CATALOGS]',
  initialState: catalogsAdapter.getInitialState({
    treeData: [],
    treeDataForUpload: [],
    loaded: false,
  }),
  reducers: {},
  extraReducers: {
    [fetchCatalogs.fulfilled]: (state, { payload }) => {
      const treeData = prepareTreeData(payload);
      const treeDataForUpload = prepareTreeDataForUpload(payload);
      state.treeData = treeData;
      state.treeDataForUpload = treeDataForUpload;
      state.loaded = true;
    },
  },
});

const prepareTreeData = (catalogs) => {
  return catalogs.map((catalog) => {
    const { subCatalogs, documents, name, documentName, ...rest } = catalog;
    const children = [
      ...(subCatalogs && subCatalogs.length ? [...subCatalogs] : []),
    ];
    return {
      title: name || documentName,
      ...(subCatalogs && {
        children: prepareTreeData(children),
        isDirectory: true,
      }),
      documents,
      expanded: true,

      ...rest,
    };
  });
};

const prepareTreeDataForUpload = (catalogs) => {
  return catalogs.map((catalog) => {
    const { subCatalogs, documents, name, documentName, ...rest } = catalog;
    const children = [
      ...(subCatalogs && subCatalogs.length ? [...subCatalogs] : []),
      ...(documents && documents.length ? [...documents] : []),
    ];
    return {
      title: name || documentName,
      ...((subCatalogs || documents) && {
        children: prepareTreeDataForUpload(children),
        isDirectory: true,
      }),
      expanded: true,
      ...rest,
    };
  });
};

export default catalogsSlice.reducer;
