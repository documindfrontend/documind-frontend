import { createAsyncThunk } from '@reduxjs/toolkit';
import { catalogService } from 'app/services/catalog-service';

export const fetchCatalogs = createAsyncThunk(
  '[CATALOGS] FETCH_CATALOGS',
  async () => {
    return await catalogService().getCatalogs();
  }
);




export const postGuidsForSign = createAsyncThunk(
  '[CATALOGS] SEND_GUIDS_FOR_SIGN',
  async (id) => {
    return await catalogService().sendGuidsForSign(id);
  }
);

export const sendGuidsForSignForMany = createAsyncThunk(
  '[CATALOGS] SEND_MANY_GUIDS_FOR_SIGN',
  async (id) => {
    return await catalogService().sendGuidsForSignForMany(id);
  }
);

export const deleteCatalog = createAsyncThunk(
  '[CATALOGS] DELETE_CATALOG',
  async (id) => {
    return await catalogService().deleteCatalog(id);
  }
);

export const deleteDocument = createAsyncThunk(
  '[CATALOGS] DELETE_DOCUMENT',
  async (id) => {
    return await catalogService().deleteDocument(id);
  }
);

export const addNewCatalog = createAsyncThunk(
  '[CATALOGS] ADD_NEW_CATALOG',
  async (id) => {
    return await catalogService().addNewCatalog(id);
  }
);
