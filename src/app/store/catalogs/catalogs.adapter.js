import { createEntityAdapter } from '@reduxjs/toolkit';

const catalogsAdapter = createEntityAdapter({
  selectId: (catalog) => catalog.catalogId,
});

export { catalogsAdapter };
