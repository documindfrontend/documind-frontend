import { configureStore } from '@reduxjs/toolkit';

import catalogsReducer from './catalogs/catalogs.reducer';
import bulkDocumentsUploadReducer from './bulk-documents-upload/bulk-documents-upload.reducer';
import accountReducer from './account/account.reducer';

export default configureStore({
  reducer: {
    catalogs: catalogsReducer,
    bulkDocumentsUpload: bulkDocumentsUploadReducer,
    account: accountReducer,
  },
});
