import { createSlice } from '@reduxjs/toolkit';
import { bulkDocumentUploadAdapter } from './bulk-documents-upload.adapter';

import {
  getFilesGuids,
  uploadFiles,
  fetchDocumentContent,
  fetchDocument,
  fetchDocumentAcroFields,
} from './bulk-documents-upload.thunks';

export const bulkDocumentsUploadSlice = createSlice({
  name: '[BULK-DOCUMENT-UPLOAD]',
  initialState: bulkDocumentUploadAdapter.getInitialState({
    selectedFolder: {},
    uploadedFiles: [],
    uploadedCsvFile: {},
    selectedProcess: [],
    selectedDocuments: [],
    selectedDocumentData: {},
    selectedDocumentAcroFields: [],
  }),
  reducers: {
    SET_SELECTED_DOCUMENTS: (state, action) => {
      state.selectedDocuments = action.payload;
    },
    RESET_UPLOADED_FILES: (state, action) => {
      state.uploadedFiles = [];
    },
    SET_SELECTED_FOLDER: (state, action) => {
      const { catalogId, title, documents } = action.payload;
      state.selectedFolder = { title, catalogId, documents };
    },
  },
  extraReducers: {
    [getFilesGuids.fulfilled]: (state, { payload }) => {
      const fileGuids = payload.map((x) => ({ guid: x }));
      state.uploadedFiles.push(...fileGuids);
    },
    [uploadFiles.fulfilled]: (state, { payload }) => {
      const files = state.uploadedFiles.map((file) => {
        const notUploaded = payload.notUploadedFiles.includes(file.guid);
        return {
          ...file,
          uploaded: true,
          ...(notUploaded && { uploaded: false }),
        };
      });
      state.uploadedFiles = files;
    },
    [uploadFiles.rejected]: (state, { meta }) => {
      const files = state.uploadedFiles.map((file) => ({
        ...(file.guid === meta.arg.id ? { ...file, uploaded: true } : file),
      }));
      state.uploadedFiles = files;
    },
    [fetchDocumentContent.fulfilled]: (state, { payload }) => {
      state.selectedDocuments[0].documentBase64 = payload;
    },
    [fetchDocument.fulfilled]: (state, { payload }) => {
      state.selectedDocumentData = payload;
    },
    [fetchDocumentAcroFields.fulfilled]: (state, { payload }) => {
      state.selectedDocumentAcroFields = payload;
    },
  },
});
export const {
  SET_SELECTED_DOCUMENTS: setSelectedDocuments,
  RESET_UPLOADED_FILES: resetUploadedFiles,
  SET_SELECTED_FOLDER: setSelectedFolder,
} = bulkDocumentsUploadSlice.actions;

export default bulkDocumentsUploadSlice.reducer;
