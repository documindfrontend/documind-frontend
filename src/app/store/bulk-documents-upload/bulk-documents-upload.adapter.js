import { createEntityAdapter } from '@reduxjs/toolkit';

const bulkDocumentUploadAdapter = createEntityAdapter({
  selectId: (file) => file.id,
});

export { bulkDocumentUploadAdapter };
