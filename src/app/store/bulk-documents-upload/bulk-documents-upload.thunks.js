import { createAsyncThunk } from '@reduxjs/toolkit';
import { catalogService } from 'app/services/catalog-service';

export const addBulkPdfDocument = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] ADD_BULK_PDF_DOCUMENT',
  async () => {
    return await catalogService().getGuids();
  }
);

export const fetchDocumentContent = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] FETCH_DOCUMENT_CONTENT',
  async (id) => {
    return await catalogService().getDocumentContent(id);
  }
);
export const fetchDocumentAcroFields = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] FETCH_DOCUMENT_ACRO_FIELDS',
  async (id) => {
    return await catalogService().getDocumentAcroFields(id);
  }
);

export const updateDocumentAcroFields = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] UPDATE_DOCUMENT_ACRO_FIELDS',
  async (data) => {
    return await catalogService().updateDocumentAcroFields(data);
  }
);

export const fetchDocument = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] FETCH_DOCUMENT',
  async (id) => {
    return await catalogService().getDocument(id);
  }
);

export const getFilesGuids = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] GET_FILES_GUIDS',
  async (zzz) => {
    return await catalogService().getGuids(zzz);
  }
);

export const addBulkCsvDocument = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] ADD_BULK_CSV_DOCUMENT',
  async () => {
    return await catalogService().getGuids();
  }
);

export const uploadFiles = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] UPLOAD_FILES',
  async (files) => {
    return await catalogService().uploadFiles(files);
  }
);

export const createNewDocument = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] CREATE_NEW_DOCUMENT',
  async (file) => {
    return await catalogService().createNewDocument(file);
  }
);

export const addNewCatalog = createAsyncThunk(
  '[BULK-DOCUMENT-UPLOAD] ADD_NEW_FOLDER',
  async (folder) => {
    return await catalogService().addNewCatalog(folder);
  }
);

// export const addProcess = createAsyncThunk(
//   '[BULK-DOCUMENT-UPLOAD] ADD_PROCESS',
//   async () => {
//     return await catalogService().getGuids();
//   }
// );
