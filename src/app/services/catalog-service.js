import ApiError from './api.error';

export const catalogService = () => {
  const baseUrl = 'https://test.documind.co:447/api/react-bff/api';
  // const baseUrl = 'https://demo.documind.co:449/api';

  const getPostHeaders = () => {
    var accessToken = sessionStorage.getItem('access_token');

    return new Headers({
      Authorization: 'Bearer ' + accessToken,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });
  };

  // --------!
  const getCatalogs = async () => {
    var accessToken = sessionStorage.getItem('access_token');

    const res = await fetch(`${baseUrl}/Catalog`, {
      method: 'GET',
      headers: new Headers({
        Authorization: 'Bearer ' + accessToken,
      }),
    });
    if (!res.ok) {
      throw new ApiError('Catalogs fetch failed', {
        url: `${baseUrl}/Catalog`,
        status: res.status,
        statusText: res.statusText,
      });
    }
    const response = await res.json();
    return [response];
  };
  // --------!

  const getDocumentContent = async (selectedDocumentId) => {
    var accessToken = sessionStorage.getItem('access_token');

    const res = await fetch(
      `${baseUrl}/Documents/${selectedDocumentId}/content`,
      {
        method: 'GET',
        headers: new Headers({
          Authorization: 'Bearer ' + accessToken,
        }),
      }
    );
    if (!res.ok) {
      throw new ApiError('Catalogs fetch failed', {
        url: `${baseUrl}/Catalog`,
        status: res.status,
        statusText: res.statusText,
      });
    }
    const responseBase64 = await res.text();
    return responseBase64;
  };
  // --------!
  const getDocument = async (selectedDocumentId) => {
    var accessToken = sessionStorage.getItem('access_token');

    const res = await fetch(
      `${baseUrl}/Documents/${selectedDocumentId}/getDocument`,
      {
        method: 'GET',
        headers: new Headers({
          Authorization: 'Bearer ' + accessToken,
        }),
      }
    );
    if (!res.ok) {
      throw new ApiError('getDocument fetch failed', {
        url: `${baseUrl}/getDocument`,
        status: res.status,
        statusText: res.statusText,
      });
    }
    const response = await res.json();
    return response;
  };
  // --------

  const deleteCatalog = async (id) => {
    var accessToken = sessionStorage.getItem('access_token');

    const url = `${baseUrl}/Catalog?catalogId=${id}`;
    const res = await fetch(url, {
      method: 'DELETE',
      headers: new Headers({
        Authorization: 'Bearer ' + accessToken,
      }),
    });

    if (!res.ok) {
      throw new ApiError('Document has not been deleted', {
        url,
        status: res.status,
        statusText: res.statusText,
      });
    }
    return await res.json();
  };
  // --------!
  const getGuids = async (quantity) => {
    var accessToken = sessionStorage.getItem('access_token');

    const res = await fetch(`${baseUrl}/CreateDocument/getGuids/${quantity}`, {
      method: 'GET',
      headers: new Headers({
        Authorization: 'Bearer ' + accessToken,
      }),
    });

    if (!res.ok) {
      throw new ApiError('Catalogs getGuids failed', {
        url: `${baseUrl}/CreateDocument/getGuids/${quantity}`,
        status: res.status,
        statusText: res.statusText,
      });
    }
    const response = await res.json();
    return response;
  };

  // ---getAcroFields-----!
  const getDocumentAcroFields = async (quantity) => {
    var accessToken = sessionStorage.getItem('access_token');

    const res = await fetch(`${baseUrl}/AcroFields/listAcros/${quantity}`, {
      method: 'GET',
      headers: new Headers({
        Authorization: 'Bearer ' + accessToken,
      }),
    });

    if (!res.ok) {
      throw new ApiError('Catalogs getGuids failed', {
        url: `${baseUrl}/CreateDocument/getGuids/${quantity}`,
        status: res.status,
        statusText: res.statusText,
      });
    }
    const response = await res.json();
    return response;
  };

  // ---UPDATE AcroFields-----!

  const updateDocumentAcroFields = async ({
    docGuid,
    namesValues,
    newFileName,
  }) => {
    const bodyData = {
      namesValues,
      newFileName,
    };

    try {
      const res = await fetch(`${baseUrl}/AcroFields/setAcroValue/${docGuid}`, {
        method: 'POST',
        headers: getPostHeaders(),
        body: JSON.stringify(bodyData),
      });
      const response = await res.text();
      return response;
    } catch (error) {
      throw error;
    }
  };

  // --------!
  const uploadFiles = async (files) => {
    var accessToken = sessionStorage.getItem('access_token');

    const formData = new FormData();
    files.forEach((file) => {
      formData.append('files', file);
      formData.append('guids', file.id);
    });

    const url = `${baseUrl}/CreateDocument/uploadFiles`;
    // console.log('upload progress:'+ Math.round(ProgressEvent.loaded / ProgressEvent.total *100 )+'%')
    try {
      const res = await fetch(url, {
        method: 'POST',
        headers: new Headers({
          Authorization: 'Bearer ' + accessToken,
        }),
        body: formData,
      });
      const response = await res.json();
      return response;
    } catch (error) {
      throw error;
    }
  };
  // --------!
  const sendGuidsForSign = async (files) => {
    // const formData = new FormData();
    // files.forEach((file) => {
    //   file.documentId;
    // });

    // try {
    const res = await fetch(`${baseUrl}/MailSender/sendForSign`, {
      method: 'POST',
      headers: getPostHeaders(),
      body: JSON.stringify([files]),
    });
    const response = await res.json();
    return response;
    // } catch (error) {
    //   throw error;
    // }
  };
  // --------!
  const sendGuidsForSignForMany = async (files) => {
    const res = await fetch(`${baseUrl}/MailSender/sendForSign`, {
      method: 'POST',
      headers: getPostHeaders(),
      body: JSON.stringify(files),
    });
    const response = await res.json();
    return response;
  };
  // --------!
  const addNewCatalog = async (folder) => {
    const res = await fetch(`${baseUrl}/Catalog`, {
      method: 'POST',
      headers: getPostHeaders(),
      body: JSON.stringify(folder),
    });
    if (!res.ok) {
      throw new ApiError('New catalog has not been created', {
        url: `${baseUrl}/Catalog`,
        status: res.status,
        statusText: res.statusText,
      });
    }
    return await res.json();
  };
  // --------!
  const createNewDocument = async (document) => {
    const _usersResouceUrl = `${baseUrl}/CreateDocument/createNewDocument`;
    const res = await fetch(_usersResouceUrl, {
      method: 'POST',
      headers: getPostHeaders(),
      body: JSON.stringify(document),
    });
    if (!res.ok) {
      throw new ApiError('New document has not been created', {
        url: _usersResouceUrl,
        status: res.status,
        statusText: res.statusText,
      });
    }
    return await res.json();
  };

  // ========

  return {
    getCatalogs,
    getDocumentContent,
    getDocument,
    sendGuidsForSign,
    sendGuidsForSignForMany,
    deleteCatalog,
    getGuids,
    uploadFiles,
    createNewDocument,
    addNewCatalog,
    getDocumentAcroFields,
    updateDocumentAcroFields,
  };
};
