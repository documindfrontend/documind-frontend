import { UserManager } from 'oidc-client';

import { oidcSettings } from 'configuration/oidcSettings';

export default class AuthService {
  constructor() {
    this.userManager = new UserManager(oidcSettings);
  }

  async initialize(signInCallback) {
    let signIn2 = () => this.signIn();

    this.userManager
      .signinRedirectCallback()
      .then(function () {
        signInCallback();
      })
      .catch(function (e) {
        signIn2();
      });
  }

  signIn() {
    this.userManager.signinRedirect();
  }

  async signOut() {
    await this.userManager.signoutRedirect();
  }

  async getUser() {
    return await this.userManager.getUser();
  }

  async getAccessToken() {
    const user = await this.userManager.getUser();
    return user && user.access_token;
  }
}

// export const AuthService = () => {
//   let user;
//   const signIn = () => {
//     const userManager = new UserManager(oidcSettings);

//     userManager
//       .signinRedirectCallback()
//       .then((user) => {
//         // resolve(user);
//       })
//       .catch((xx) => {
//         userManager.signinRedirect();
//       });
//   };

//   const signOut = () => {
//     const userManager = new UserManager(oidcSettings);
//     userManager.signoutRedirect();
//   };

//   const getAccessToken = async () => {
//     const userManager = new UserManager(oidcSettings);
//     const user = await userManager.getUser();
//     return user && user.access_token;
//   };

//   const getUser = async () => {
//     const userManager = new UserManager(oidcSettings);
//     if (!user) {
//       user = await userManager.getUser();
//     }
//     return user;
//   };

//   return {
//     signIn,
//     getAccessToken,
//     signOut,
//     getUser,
//   };
// };
