// export const oidcSettings = {
//   authority: 'https://demo.documind.co:446/',
//   client_id: 'documind_react_client',
//   redirect_uri: 'http://localhost:3001',
//   response_type: 'code',
//   response_mode: 'query',
//   scope: 'openid profile documind_react_client_bff',
//   post_logout_redirect_uri: 'http://localhost:3001',
// };

export const oidcSettings = {
 authority: 'https://test.documind.co:446/',
 client_id: 'documind_react_client',
 redirect_uri: 'http://localhost:3001',
 response_type: 'code',
 response_mode: 'query',
 scope: 'openid profile documind_react_client_bff',
 post_logout_redirect_uri: 'http://localhost:3001',
};

// export const oidcSettings = {
//   authority: 'https://demo.documind.co:446/',
//   client_id: 'documind_react_client',
//   redirect_uri: 'https://demo.documind.co',
//   response_type: 'code',
//   response_mode: 'query',
//   scope: 'openid profile documind_react_client_bff',
//   post_logout_redirect_uri: 'https://demo.documind.co',
//  };